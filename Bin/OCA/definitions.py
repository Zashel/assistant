import flask
import matplotlib as mpl
import numpy as np
import os
import pandas as pd
import traceback
from Mordor import Council
from MordorManager import get_api
from scipy import stats
from string import Template


class Locale(dict):
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            return item


PATH = os.path.join(os.environ["APPDATA"], "OCA")
REMOTE_PATH = r"//172.19.64.20\cobros\Assistant\Bin\OCA"
CONFIG_PATH = os.path.join(REMOTE_PATH, "config")
TEMPLATING_PATH = os.path.join(REMOTE_PATH, "Templating")
STATIC_PATH = os.path.join(REMOTE_PATH, "static")
LOCAL_PATH = os.path.join(os.environ["LOCALAPPDATA"], "OCA")
if not os.path.exists(PATH):
    os.makedirs(PATH)
    

LOCALE_FILE = Council(None, name="Locale", path=PATH, remote_path=CONFIG_PATH)
CONFIG = Council(None, name="Config", path=PATH, remote_path=CONFIG_PATH)
MODULES = Council(None, name="Modules", path=PATH, remote_path=CONFIG_PATH)

DEBUG_FLAG = True
LOCALE = Locale(LOCALE_FILE)
#LOCALE = LOCALE_FILE <--- this way works

PROGRAMDATA = os.path.join(os.environ["PROGRAMDATA"], "Senpai")
if not os.path.exists(PROGRAMDATA):
    os.makedirs(PROGRAMDATA)
    
    
DATA_TEMPLATE = """# Python Code
# You may yield each row as a dictionary with keys related to field name in html
# IE for a list of fields called name_0, name_1, name_2, visible_0, visible_1, visible_2...:
# for module in MODULES:
#    yield {"name": module, "visible": MODULES[module]["name"]}
"""

EXECUTE_TEMPLATE = """# Python Code
# To enhance readibility you may yield a Signal method.
# signal.message(message) being message an html formatted text. This is not a termination signal.
# signal.table(headers, data, order) being headers a list of tuples of (name, visible), data a list of dicts with name as key,
#   order a list of tuples with (name, asc/desc)
# signal.download(file) being file a temp file to download in navigator
# signal.error(error description)
# signal.info(info) a termination info html formatted string
# signal.redirect(href)
# All the data requested is unred request object as in flask.Request Object documentation
"""


def save_council(what):
    what = what.title( ) # Validate it anyway
    sql = dict(SQL)
    verif = {"Config": CONFIG, "Locale": LOCALE, "Modules": MODULES}
    assert what in verif
    with open(os.path.join(SENPAI_REMOTE_PATH, what), "w", encoding="iso-8859-15") as f:
                json.dump(verif[what], f)
                
                
class Signal: #TODO: Signal
    def __init__(self, signal_type, data):
        self.signal_type = signal_type
        self.data = data
        
    @classmethod
    def download(cls, path):
        return cls("download", path)
        
    @classmethod
    def error(cls, description):
        return cls("description", description)
        
    @classmethod
    def info(cls, description):
        return cls("info", description)

    @classmethod
    def message(cls, message):
        return cls("message", message)
        
    @classmethod
    def redirect(cls, href):
        return cls("redirect", href)
    
    @classmethod
    def table(cls, headers, data, order):
        return cls("table", (headers, data, order))

                
class Data(dict):
    def __getitem__(self, item):
        if self.active_section is None:
            if item in self:
                return dict.__getitem__(self, item)
            else:
                return None
        else:
            if "_" in item and item.split("_")[-1].isnumeric():
                key, count = "_".join(item.split("_")[:-1])+"_{}", int(item.split("_")[-1])
            else:
                key, count = item, None
            if key in self.fields:
                if "data" not in dict.__getitem__(self, self.active_section):
                    if count is not None:
                        dict.__getitem__(self, self.active_section)["data"] = []
                    else:
                        dict.__getitem__(self, self.active_section)["data"] = {}
                    if count is None and key not in dict.__getitem__(self, self.active_section)["data"]:
                        dict.__getitem__(self, self.active_section)["data"][key] = []
                if count is not None and count < len(dict.__getitem__(self, self.active_section)["data"]):
                    dalist = dict.__getitem__(self, self.active_section)["data"][count]
                    return dalist[self.fields.index(key)]
                elif count is None:
                    try:
                        return [i in item for i in dict.__getitem__(self, self.active_section)["data"][key] if i is not None]
                    except TypeError:
                        print(f"TYPEERROR: {item} {self.active_section} {key} {dict.__getitem__(self, self.active_section)['data']} {self.fields}")
                        raise
            if item in self.rows:
                return dict.__getitem__(self, self.active_section)[item]
                    
    def __setitem__(self, key, item):
        print(f"SET {item} to {key}")
        if self.active_section is None:
            dict.__setitem__(self, key, item)
        else:
            if "_" in key and key.split("_")[-1].isnumeric():
                key, count = "_".join(key.split("_")[:-1])+"_{}", int(key.split("_")[-1])
            else:
                count = key
            print(key, count)
            if key in self.fields:
                if "data" not in dict.__getitem__(self, self.active_section):
                    if count is not None:
                        dict.__getitem__(self, self.active_section)["data"] = []
                    else:
                        dict.__getitem__(self, self.active_section)["data"] = {}
                if count is not None and count < len(dict.__getitem__(self, self.active_section)["data"]):
                    dalist = dict.__getitem__(self, self.active_section)["data"][count]
                    dalist[self.fields.index(key)] = item
                    print(dalist)
                elif count is not None and count >= len(dict.__getitem__(self, self.active_section)["data"]): 
                    dalist = [None for x in range(len(self.fields))]
                    dalist[self.fields.index(key)] = item
                    dict.__getitem__(self, self.active_section)["data"].append(dalist)
                elif count is None:
                    dict.__getitem__(self, self.active_section)["data"][key] = item
                    
    def __str__(self):
        return self.to_dict().__str__()
            
    @property
    def active_section(self):
        if hasattr(self, "_active_section"):
            return self._active_section
        else:
            return None
    
    @active_section.setter
    def active_section(self, section):
        self.set_active_section(section)
        
    @property
    def combos(self):
        if not hasattr(self, "_combos"):
            self._combos = {}
        return self._combos            
        
    @property
    def counting_rows(self):
        self.to_dict()
        return self._counting_rows
        
    @property
    def fields(self):
        if self.active_section is not None:
            definition = dict.__getitem__(self, self.active_section)
            if "sections" in definition:
                final = []
                for section in definition["sections"]:
                    for row in section["row"]:
                        final.extend([item["name"] for item in row["items"]])
                return final
        
    @property
    def rows(self):
        if self.active_section is not None:
            definition = dict.__getitem__(self, self.active_section)
            if "sections" in definition:
                final = []
                for section in definition["sections"]:
                    for row in section["row"]:
                        if "items" in row:
                            final.extend([item["name"] for item in row["items"]])
                print(final)
                return final
                
    @property
    def fields_for_rows(self):
        if self.active_section is not None:
            definition = dict.__getitem__(self, self.active_section)
            if "sections" in definition:
                final = {}
                for section in definition["sections"]:
                    for field in section["row"]:
                        if "items" in field:
                            for row in field["items"]:
                                final[row["name"]] = field["id"]
                print(f"ROWS FOR SECTION {final}")
                return final
            
    def append(self, item):
        if self.active_section is not None:
            f_index = -1
            if "data" not in dict.__getitem__(self, self.active_section):
                    dict.__getitem__(self, self.active_section)["data"] = []
            dalist = [key.strip("_{}") in item and item[key.strip("_{}")] or None for key in self.fields]
            for index, old_item in enumerate(dict.__getitem__(self, self.active_section)["data"]):
                if all([key.strip("_{}") not in item or (key.strip("_{}") in item and self[key.format(index)]) is None for key in self.fields]):
                    f_index = index
                    for key in self.fields:
                        if key.strip("_{}") in item:
                            self[key.format(index)] = item[key.strip("_{}")]
                    break
            if f_index == -1 and not all([i is None for i in dalist]):
                dict.__getitem__(self, self.active_section)["data"].append(dalist)
            
    def load(self, _parent, _module, _section, data):
        if data is None:
            data = []
        keys = [item.strip("<>") for item in _section.strip("/").split("/")]  
        file = os.path.join(REMOTE_PATH, "modules", _parent, _module, *keys, "data.py")
        if not os.path.exists(file):
            os.makedirs(os.path.split(file)[0])
            with open(file, "w") as f:
                f.write(DATA_TEMPLATE)
        self.set_section(_section, MODULES[_parent]["modules"][_module][_section])
        code = "" # Each key is a variable now
        code = "\n".join((code, "def generate(self, _parent, _module, _section, args):"))
        code = "\n".join((code, "\n".join([f"    {key} = {data[key].__repr__()}" for key in data if isinstance(key, str)]), ""))
        raw_code = []
        with open(file) as f:
            for row in f:
                code = "    ".join((code, row))
                raw_code.append(row)
        final_code = "for item in generate(self, _parent, _module, _section, data):\n    if item is not None and isinstance(item, dict):\n        item.update(dict([(key, value is True and 1 or 0) for key, value in item.items() if isinstance(value, bool)]))\n        self.append(item)"
        code = "\n".join((code, "    yield None", final_code))
        print(code)
        try:
            exec(code)
        except Exception as e:
            raise e #TODO -> Check exception and show it in browser
            
    def set_active_section(self, section):
        if section in self:
            self._active_section = section
            
    def set_combo(self, item, combo):
        """
        item: Combo item to set combo
        combo: list of list of value, visible string.
        """
        if not hasattr(self, "_combos"):
            self._combos = {}
        self._combos[item] = combo
            
    def set_section(self, section_id, section_data): # Section Data as Modules text file -> I'm confusing names
        self._active_section = None
        self[section_id] = {"id": section_id}
        self[section_id].update(section_data)
        self.set_active_section(section_id)
        
    def to_dict(self):
        if self.active_section is None:
            return dict.__str__(self)
        else:
            final = {}
            self._counting_rows = {}
            fields_for_rows = self.fields_for_rows
            if "data" not in dict.__getitem__(self, self.active_section):
                dict.__getitem__(self, self.active_section)["data"] = []
            for index, item in enumerate(dict.__getitem__(self, self.active_section)["data"]):
                for field in self.fields:
                    if not field.endswith("_{}"):
                        if not field in final:
                            final[field] = []
                        print(f"FIELD: {item[self.fields.index(field)]}")
                        if isinstance(item[self.fields.index(field)], (list, tuple)):
                            final[field].append([i for i in item[self.fields.index(field)] if i is not None])
                        elif item[self.fields.index(field)] is not None:
                            final[field].append(item[self.fields.index(field)])
                    elif item[self.fields.index(field)] is not None:
                        self._counting_rows[fields_for_rows[field]] = index + 1
                        final[field.format(index)] = item[self.fields.index(field)]
            print(f"FINAL: {final}")
            return final
    
    
class Module:
    def __init__(self, directory, identifier, args, new=False):
        self.directory = directory
        self.identifier = identifier
        self.args = args
        self._dict_args = None
        self._possibles = None
        self._valkeys = None
        self.executing = False
        self.response = None
        if new is True:
            if self.directory not in MODULES:
                MODULES[self.directory] = {"name": self.directory.title(), "modules": []}
            if self.identifier not in MODULES[self.directory]:
                MODULES[self.directory]["modules"][self.identifier] = {"name": self.identifier.title(), "routes": {"/", {"sections": []}}}
            save_council(what)
        self.data = {} # As with modules: section -> list of fields. Every field with the name it may have in the form
        
    @property
    def dict_args(self):
        if self._dict_args is None:
            self._dict_args = dict(zip(self.valkeys, self.args))
        return self._dict_args
        
    @property
    def possibles(self):
        if self._possibles is None: # Execute it only once just when needed
            keys = list(MODULES[self.directory]["modules"][self.identifier].keys())
            possibles = []
            for key in keys:
                key_val = key.strip("/").split("/")
                if len(key_val) == 1 and key_val[0] == "":
                    key_val = []
                if len(key_val) == len(self.args):
                    possibles.append(key)
            while len(possibles) > 1:
                for i, possible in enumerate(list(possibles)):
                    for index, key in enumerate(possible.strip("/").split("/")):
                        if "<" not in key and ">" not in key and self.args[index] != key:
                            del(possibles[i])
                            break
            self._possibles = possibles
        return self._possibles
        
    @property
    def valkeys(self):
        if self._valkeys is None:
            if len(self.possibles) > 0:
                self._valkeys = [item.strip("<>") for item in self.possibles[0].strip("/").split("/")]
            else:
                self._valkeys = []
        return self._valkeys
        
    def get_data(self, pth, data):
        self.data = Data()
        self.data.load(self.directory, self.identifier, pth, data)
        return self.data
        
    def execute(self, *, request=None):
        self.executing = True
        print(os.path.join(REMOTE_PATH, "modules", self.directory, self.identifier, *[item.strip("<>") for item in self.possibles[0].strip("/").split("/")]))
        file = os.path.join(REMOTE_PATH, "modules", self.directory, self.identifier, *[item.strip("<>") for item in self.possibles[0].strip("/").split("/")], "execute.py")
        if not os.path.exists(os.path.split(file)[0]):
            os.makedirs(os.path.split(file)[0])
        if not os.path.exists(file):
            with open(file, "w") as f:
                f.write(EXECUTE_TEMPLATE)
        #TODO: Execute -> Begin with redirect
        with open(os.path.join(os.path.split(__file__)[0], "execute_snip.py")) as f:
            code = f.read()
        with open(file) as f:
            fcode = ""
            for row in f:
                fcode = "        ".join((fcode, row))
        code = code.format(fcode, "{}")
        print(code)
        exec(code)
        self.executing = False
        return self.response
        
    def execute_signal(self, signal):
        print("GOT MY SIGNAL")
        if isinstance(signal, pd.DataFrame):
            self.response = flask.render_template("base.html", MODULES=MODULES, info=signal.to_html(border=0, classes="view_table"))
        elif signal.signal_type == "info":
            self.response = flask.render_template("base.html", MODULES=MODULES, info=signal.data)
        elif signal.signal_type == "redirect":
            self.response = flask.redirect(signal.data)
        else:
            self.response = flask.render_template("base.html", MODULES=MODULES, info=f"Nothing to show {type(signal)}")
    
    def render(self):
        if len(self.possibles) > 0:
            template = MODULES[self.directory]["modules"][self.identifier][self.possibles[0]]
            self.get_data(self.possibles[0], self.dict_args)
            combos = {}
            for section in template["sections"]:
                for row in section["row"]:
                    for item in row["items"]:
                        if item["type"] == "combo":
                            combos[item["name"].strip("_{}")] = eval(item["values"])
        else:
            valkeys = []
            template = {}
            self.get_data("/", None)
            combos = {}
        return {"directory": self.directory, "identifier": self.identifier, "args": self.dict_args, "template": template, "data": self.data,
               "action": f"/modules/{self.directory}/{self.identifier}/{Template(self.possibles[0][1:].replace('<', '$').replace('>', '')).safe_substitute(self.dict_args)}",
               "combos": combos, "rows": self.data.counting_rows, "Template": Template}
