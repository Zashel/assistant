import traceback

request = request
module = self
Signal = Signal
pd, np, stats, mpl = pd, np, stats, mpl
MODULES = MODULES


def generate(request, module):
    try:
{}
    except Exception as e:
        import re
        text = traceback.format_exc().replace('\n', '<br>')
        with open(os.path.join(os.path.split(__file__)[0], "execute_snip.py")) as file_:
            data = file_.read().split("\n")
        data = [i.strip("\r") for i in data]
        data_index = data.index("{}")
        line = int(re.findall(r"line ([\d]+)", text)[0])
        text = re.sub(r"line [\d]+,", "line "+str(line - data_index), text)
        text = text.replace("File \"" , "\"Module "+module.possibles[0].replace("<", "&lt;").replace(">", "&gt;"))
        text = text.replace('generate', "execute")
        yield Signal.info(text)
        return
    else: yield Signal.info('To be Implemented')
        

for item in generate(request, module):
    if isinstance(item, (Signal, pd.DataFrame)):
        module.execute_signal(item)
    if module.executing is False: 
        break    