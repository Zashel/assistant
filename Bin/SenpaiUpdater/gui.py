import os
from tkinter import *
from tkinter.ttk import *
tkButton = Button
import win32api
from zashel.utils import daemonize
from tkutils import *
from Mordor import Council
from tkinter import tix
from functools import partial
from .senpaiupdater import FileCheck,FileUpdate


PATH = os.path.join(os.environ["APPDATA"], "SenpaiUpdater")
REMOTE_PATH = r"//172.19.64.20/cobros/Assistant/"
LOCALPATH = r"C:\Users\ops28iu2\Desktop\Assistant"
SENPAIUPDATERPATH = r"C:\Users\ops28iu2\Desktop\Assistant\Bin\SenpaiUpdater"
if not os.path.exists(PATH):
    os.makedirs(PATH)
APPS = Council(None, name="Updater", path=PATH, remote_path=SENPAIUPDATERPATH)



TITLE = "Senpai Updater"
ROOT = tix.Tk()
ROOT.title(TITLE)
tkFrame = Frame
filecheck = FileCheck()
fileupdate = FileUpdate()

class Frame(tix.Frame):
    """
    Frame for the app.
    """
    def __init__(self, master):
        tix.Frame.__init__(self, master=master)

    def destroy(self):
        tix.Frame.destroy(self)

class MainFrame(Frame):
    """
    Main Frame
    """
    def __init__(self, master):
        self.apps = dict()
        self.widgets = dict()
        self.variables = dict()
        self.labels = dict()
        '''
        for app in APPS:
            self.apps[app] = []
            localfolder = os.path.join(LOCALPATH,APPS[app]["local"])
            remotefolder = os.path.join(REMOTE_PATH,APPS[app]["remote"])
            self.apps[app] = filecheck.initialize(localfolder,remotefolder)
            self.widgets[app] = dict()
            self.variables[app] = dict()

            for file in self.apps[app]:
                self.variables[app][file] = BooleanVar()
                self.widgets[app][file] = Checkbutton(master, text=file, variable=self.variables[app][file])
        i = 0
        for app in self.widgets:
            upperpoints = [0, 0]
            up_index = upperpoints.index(min(upperpoints))
            self.labels[app] =  Label(master, text=app, font='Helvetica 15 bold').place(relx=0.05+i,rely=0.05, anchor = W)
            for widgets in self.widgets[app]:
                self.widgets[app][widgets].place(relx=0.05+i,rely=0.15 + upperpoints[up_index], anchor = W)
                upperpoints[up_index] += 0.05
            i += 0.25
        '''
        self.main = master
        self.main.config(height=600, width=1000)
        self.load_widgets()
        Button(master, text="Guardar", command=partial(self.update_selected)).place(relx=0.5, rely=0.9, anchor=N)
        Frame.__init__(self, master)

        print(self.apps)

    def update_selected(self):
        '''
        overwrite the selected files
        :return: none
        '''
        for app in APPS:
            for file in self.apps[app]:
                if self.variables[app][file].get() == True:
                    print(f"app {app} file {file}")
                    localfolder = os.path.join(LOCALPATH, APPS[app]["local"])
                    remotefolder = os.path.join(REMOTE_PATH, APPS[app]["remote"])
                    fileupdate.backupfile(file,remotefolder)
                    fileupdate.updatefile(file,localfolder,remotefolder)
        self.reload()

    def reload(self):
        '''
        refresh the widgets
        :return: none
        '''
        for app in self.widgets:
            for widget in self.widgets[app]:
                self.widgets[app][widget].place_forget()
        self.widgets = dict()
        self.labels = dict()
        self.load_widgets()

    def load_widgets(self):
        for app in APPS:
            self.apps[app] = []
            localfolder = os.path.join(LOCALPATH,APPS[app]["local"])
            remotefolder = os.path.join(REMOTE_PATH,APPS[app]["remote"])
            self.apps[app] = filecheck.initialize(localfolder,remotefolder)
            self.widgets[app] = dict()
            self.variables[app] = dict()

            for file in self.apps[app]:
                self.variables[app][file] = BooleanVar()
                self.widgets[app][file] = Checkbutton(self.main, text=file, variable=self.variables[app][file])
        i = 0
        for app in self.widgets:
            upperpoints = [0, 0]
            up_index = upperpoints.index(min(upperpoints))
            self.labels[app] =  Label(self.main, text=app, font='Helvetica 15 bold').place(relx=0.05+i,rely=0.05, anchor = W)
            for widgets in self.widgets[app]:
                self.widgets[app][widgets].place(relx=0.05+i,rely=0.15 + upperpoints[up_index], anchor = W)
                upperpoints[up_index] += 0.05
            i += 0.20




app = MainFrame(ROOT)
app.mainloop()


