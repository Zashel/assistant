import os
import datetime
import shutil
from threading import Thread


class FileCheck():
    '''
    Class to check the last update
    Use for compare the time when the file have the last update
    '''
    def initialize(self, localpath, remotepath):
        self.local_files = self.scan_folder(localpath)
        self.remote_files = self.scan_folder(remotepath)
        self.updatable_files = []
        self.compare_dates()
        return self.updatable_files

    def scan_folder(self,folderpath):
        '''
        :param folderpath: path to folder for scan.
        :return: files_list: dict with the path and date of the files.
        '''
        files_list = dict()
        for file in os.listdir(folderpath):
            filepath = os.path.join(folderpath,file)
            filedate = datetime.datetime.fromtimestamp(os.path.getmtime(filepath))
            files_list[file] = {"path": filepath, "date": filedate}
        return files_list

    def compare_dates(self):
        for file in self.local_files.keys():
            if file in self.remote_files.keys():
                if self.local_files[file]["date"] > self.remote_files[file]["date"]:
                    self.updatable_files.append(file)
            else:
                self.updatable_files.append(file)

class FileUpdate():
    '''
    Class to overwrite the updatable files with the new content
    '''
    def backupfile(self,file,path):
        print(f"creating backup for: {file}")
        backupfolder = path.replace("Assistant","BackupAssistant")
        now = datetime.datetime.now()
        datestr = os.path.join(backupfolder,now.strftime("%Y%m%d %H%M%S"))
        if not os.path.exists(backupfolder):
            os.mkdir(backupfolder)
        else:
            print("folder ok")
        bfile = datestr + " " + file
        print(file)
        remotefile = os.path.join(path,file)
        if os.path.exists(remotefile):
            backupfile = os.path.join(backupfolder,bfile)
            print(remotefile)
            print(backupfile)
            shutil.copyfile(remotefile,backupfile)
        else:
            print("El archivo es nuevo, por lo que no se realiza backup de anteriores versiones")

    def updatefile(self,file,localpath, remotepath):
        remotefile = os.path.join(remotepath,file)
        localfile = os.path.join(localpath,file)
        print(localfile)
        print(remotefile)
        shutil.copyfile(localfile,remotefile)



