import csv
import datetime
import os
import subprocess
import getpass
from tkinter import *
from tkinter import filedialog
from tkinter.ttk import *
import win32api
tkButton = Button
from tkinter import tix
from Calendarpy import *
from .definitions import CONFIG
from events import FirstLightOnTheFifthDay
from events import Event as Balrog # There is already an "Event" in tkinter.
from multiprocessing import Lock
from functools import partial
from tkutils import *
from zashel.utils import daemonize
from adohelper import Where
from tkadolink import ADODB, ADOTree, Tk, mainloop


print("imported everything")

TITLE = "Senpai BO Edition"

SENPAIPATH = os.path.dirname(os.path.abspath(__file__)) # A present for you
LoggedInEvent = Balrog("logged_in")
SplashInEvent = Balrog("splash_in")
SplashOutEvent = Balrog("splash_out") # _event_splash_out
print("events generated")

ROOT = tix.Tk()
#This configures the graphical interface
ROOT.title(TITLE)  #Name of the window

print("root created")

#This is done this way to show a Splash screen while loading.
tkFrame = Frame
class Frame(tix.Frame, FirstLightOnTheFifthDay):
    """"copy_on_click": "Copiar en un click"
    Base frame to listen to Gandalf Events.
    """
    def __init__(self, master):
        tix.Frame.__init__(self, master=master, **APPEARANCE)
        FirstLightOnTheFifthDay.__init__(self, True)
        self.logo_senpai = tix.PhotoImage(file=os.path.join(SENPAIPATH, "senpailogo.gif"))
        self.logo_transcom = tix.PhotoImage(file=os.path.join(SENPAIPATH, "transcomlogo.gif"))
        self.logos = {}
        for service in CONFIG.LOGO_SERVICES:
            self.logos[service] = tix.PhotoImage(file=os.path.join(SENPAIPATH, CONFIG.LOGO_SERVICES[service]))
        
    def destroy(self):
        self.do_out()
        tix.Frame.destroy(self)
        

tkWindow = Window
class Window(tkWindow, FirstLightOnTheFifthDay):
    """
    Windows class for nonmodal widows over root.
    """
    def __init__(self, master, *, title=None, data=None):     
        tkWindow.__init__(self, master, title=title, data=data)
        FirstLightOnTheFifthDay.__init__(self)
        
    def destroy(self):
        self.do_out()
        tkWindow.destroy(self)


class NotifyErrorWindow(Window):
    '''
    Window to show a entry to notify an error to developers
    '''
    def __init__(self, master, program="SenpaiBO"):
        '''
        Initializes the commitments Altitude windows advice
        :param master:
        '''
        Window.__init__(self, master,title="Notificar Error")
        self.errorvar = StringVar()
        self.frame = Frame(self)
        self.frame.pack()
        self.frame.config(height=200, width=400)
        self.program = program
        description = LabelEntry(self.frame, label="Describe el error:", textvariable=self.errorvar)
        description.place(relx=0.5, rely=0.1, anchor=N)
        Button(self.frame, text="Enviar notificación del error",command = partial(self.send_notify)).place(relx=0.5, rely=0.6, anchor=N)

    def send_notify(self):
        if self.errorvar.get() == "":
            win32api.MessageBox(0, 'Describe el error para poder solucionarlo cuanto antes.')
        else:
            data = {"insert_user": f"{os.environ['USERNAME']}@{os.environ['COMPUTERNAME']}",
                    "insert_date": datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
                    "status": "PENDIENTE",
                    "program": self.program,
                    "comment": self.errorvar.get()}
            DEBUG.post(data)
            self.destroy()
        

class List_Paperwork(Window):
    """
    Window to Question Frame
    """
    def __init__(self, master, papername):
        """
        Initializes the question window
        :param master: Master for Window
        :param papername: name of the Paper
        """
        print(f"paperwork: {papername}")
        self.paperwork = papername
        #self.service, self.paper = self.paperwork[:self.paperwork.index("_")], self.paperwork[self.paperwork.index("_") + 1:]
        #print(self.service, self.paper)
        self.helper = PaperworkHelper(papername)
        self.sql = Papers[papername]
        data_source = SQL[self.paperwork]["data_source"]
        self._table = SQL[self.paperwork]["table"]
        self._path = SQL["DB_NAMES"][data_source]
        self._password = data_source in SQL["DB_PASSWORDS"] and SQL["DB_PASSWORDS"][data_source] or None
               
        self.fields = self.sql.get_insert_fields()
        text = LOCALE[papername]
        Window.__init__(self, master, title="Listado de " + text)
        
        self.withdraw()
        
        self.geometry("900x600")
        self.frame = Frame(self)
        self.frame.configure(height=600, width=800, bd=2)
        self.frame.pack()
        self.treeview = None
        self.totals = dict()
        self.order = ["Id asc"]
        self.loaded_data = list()
        self.unblock_all()

        text = LOCALE[self.paperwork]
        kwargs = {}
        if self._password is not None:
            kwargs["password"] = self._password
        adodb = ADODB(self._path, **kwargs)

        Label(self.frame, text=" ", width=2).pack()  # Chapuza for a space between widgets
        Label(self.frame, text="Listado de " + text).pack()
        Label(self.frame, text=" ", width=2).pack()  # Chapuza for a space between widgets

        locale = dict(LOCALE)
        locale.update(dict([(key, LOCALE[self.sql._back_correlations[key]]) for key in self.sql._back_correlations]))
        status = self.sql._correlations["status"]
        update_user = self.sql._correlations["update_user"]
        where = Where((status, "=", "PENDIENTE"))
        w1 = Where((update_user, "=", SENPAI.username))
        w1.And(status, "=", "BLOQUEADO")
        where.Or(w1)
        where.Or(status, "=", "SEGUIMIENTO")
        print(where.__sql__())
        def post_editing(item, values, this=self):
            update_date = datetime.datetime.now()
            old_update_date = values[this.treeview.columns.index(self.sql._correlations["update_date"])]
            go = False
            if not old_update_date or datetime.datetime.strptime(old_update_date, "%Y-%m-%d %H:%M:%S") < update_date-datetime.timedelta(minutes=5):
                this.treeview.row[self.sql._correlations["update_date"]] = update_date.strftime("%Y-%m-%d %H:%M:%S")
                values[this.treeview.columns.index(self.sql._correlations["update_date"])] = update_date.strftime("%Y-%m-%d %H:%M:%S")
                go = True
            if values[this.treeview.columns.index(self.sql._correlations["update_user"])] != SENPAI.username:
                this.treeview.row[self.sql._correlations["update_user"]] = SENPAI.username
                values[this.treeview.columns.index(self.sql._correlations["update_user"])] = SENPAI.username
                go = True
            if go is True:
                this.treeview.item(item, values=values)
        self.treeview = ADOTree(self.frame, adodb[self._table], height=20, locale=locale, 
                                #options={"__options__": {"editable": "Editable", "copy_on_click": "Copiar en un click"}}, 
                                options={"__options__": {"copy_on_click": "Copiar en un click"}}, 
                                where=where, post_editing=post_editing)
        self.columns = [key.lower() for key in self.treeview.columns]

        column_anchor = 1000 / len(self.treeview.columns)
        
        for column in self.treeview.columns:
            self.treeview.column(column, minwidth=int(column_anchor), width=100, stretch=False)  # Column may be created anyway

        self.treeview.pack()  # Copy-Paste as lifetyle
        self.treeview.bind("<Double-1>", self.on_double_click)  # checks dobleclick on treeview

        Label(self.frame, text=" ", width=2).pack()  # Chapuza for a space between widgets
        options = Frame(self)
        Button(options, text="Exportar Todo", command=self.export_data).pack(side=LEFT)
        Label(options, text=" ", anchor=E,width=5).pack(side=LEFT) #Chapuza for a space between widgets
        Button(options, text="Marcar como:", command=self.done_row).pack(side=LEFT)
        Label(options, text=" ", anchor=E, width=1).pack(side=LEFT)  # Chapuza for a space between widgets
        self.variable_status = StringVar()
        self.variable_status.set("GESTIONADO")
        combo_values = CONFIG.PAPERWORK_STATUS
        Combobox(options, values=combo_values, textvariable=self.variable_status).pack(side=LEFT)
        self.variable_comment = StringVar()
        Label(options, text=" ", anchor=E, width=3).pack(side=LEFT) #Chapuza for a space between widgets
        Label(options, text="Comentario BO:", anchor=E,width=15).pack(side=LEFT)
        Entry(options, textvariable=self.variable_comment).pack(side=LEFT)
        options.pack()
        data_frame = Frame(self)
        with self.treeview._lock:
            for item in self.treeview._data:
                status = self.treeview._data[item][self.sql._correlations["status"]]
                if status not in self.totals:
                    self.totals[status] = 0
                self.totals[status] += 1
        if "PENDIENTE" in self.totals:
            Label(data_frame, text=f"Total Pendiente: {self.totals['PENDIENTE']}").pack(side=LEFT)
            Label(options, text=" ", anchor=E, width=10).pack(side=LEFT) #Chapuza for a space between widgets
        if "SEGUIMIENTO" in self.totals:
            Label(data_frame, text=f"Total Seguimiento: {self.totals['SEGUIMIENTO']}").pack(side=LEFT)
            Label(options, text=" ", anchor=E,width=10).pack(side=LEFT) #Chapuza for a space between widgets
        data_frame.pack()
        space = Frame(self)
        Label(space, text=" ", width = 2).pack()  # Chapuza for a space between widgets
        space.pack()
        self.configure(height=600, width=800, bd=2)
        self.frame.configure(height=600, width=800, bd=2)
        
        self.deiconify()

    def __del__(self):
        self.unblock_all()
        gc.collect()

    def on_selected_row(self, event):
        time.sleep(1.5)
        self.on_single_click(event)

    def on_double_click(self, event):
        try:
            item = self.treeview.selection()[0]
        except IndexError:
            return #Out of place
        index = int(self.treeview.index(self.treeview.selection()))
        dato = self.treeview.item(item)["values"][self.columns.index(self.sql._back_correlations[self.order[0].split(" ")[0]].lower())]
        print(dato)
        self.helper.where = self.treeview.where
        self.helper.initialize_data(self.treeview._data)
        if self.treeview.indexes != []:
            self.helper.indexes = [self.treeview.item(i)["values"][0] for i in self.treeview.indexes]
        PaperworkWindow(app, self.helper, dato, order=self.order, index=index)
        self.destroy()

    def export_data(self):
        filename = filedialog.asksaveasfilename(filetypes=[("CSV separado por comas", ".csv")])
        if not filename.endswith(".csv"):
            filename += ".csv"
        with open(filename, "w") as f: # Soy muy manual
            f.write(";".join(self.treeview._columns)+"\n")
            f.write("\n".join([";".join([self.treeview._data[item][i] is not None and str(self.treeview._data[item][i]).replace("\r", "").replace("\n", " ").replace(";", ",") or "" 
                                         for i in list(self.treeview._data[item].keys())]) 
                               for item in self.treeview._data]))
            #f.write("\n".join([";".join([i is not None and str(i).replace("\r", "").replace("\n", "").replace(";", ",") or "" for i in item]) 
            #                   for item in self.treeview._data]))

    def done_row(self):
        selected_id = list()
        for item in self.treeview.selection():
            _id = int(self.treeview.item(item, "values")[0]) # ID es integral, como el pan
            selected_id.append(int(_id))
        print (selected_id)
        comment = self.variable_comment.get().strip()
        exporteddata = {"update_user": USERNAME.get(), "status": self.variable_status.get(),
                        "update_date": datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")}
        if comment: # Si no se pone comentario que se deje como está
            exporteddata["update_comment"] = comment
        exported_where = Where(("Id", "in", selected_id))
        self.sql.put(exported_where, exporteddata)
        self.destroy()
        List_Paperwork(self.master, self.paperwork)
    
    def unblock_all(self):
        unblockdata = {"update_user": "", "status": "PENDIENTE"}
        unblock_where = Where(("update_user", "=", USERNAME.get()))
        unblock_where.And("status", "=", "BLOQUEADO")
        self.sql.put(unblock_where, unblockdata)


class PaperworkWindow(Window):
    """
    Window to Paperwork
    """
    def __init__(self, master, helper, dato=0, *, order=["Id asc"], index=1, where=None):
        """
        Initializes new Paperwork window
        :param master: Master for Window
        :param paperwork: Name of Paperwork
        :param data: Data assigned to paperwork
        """
        #self.service, self.paper = paperwork[:paperwork.index("_")].lower(), paperwork[paperwork.index("_")+1:]
        #self.helper = PaperworkHelper(self.service, self.paper)
        self.paper = helper.paper
        self.helper = helper
        self._where = where
        Window.__init__(self, master, title=LOCALE[self.paper])

        # Variables
        self.data = list()
        self._id = -1
        self.dato = dato
        self.papername = helper.paper
        self.order = order
        self.new = False
        self.last_status = None
        self.index = index
        self.check_data = dict()

        #Protocol
        self.protocol("WM_DELETE_WINDOW",self.on_closing)

        #TKVars
        self.obocomment = StringVar()
        self.status = StringVar()
        self.insert_variables = dict()

        #Widgets
        self.insert_widgets = dict()
        self.frame = Frame(self)
        self.frame.pack()
        self.frame.config(height=600, width=800, bd=2)
        self.update_where = ""

        upperpoints = [0, 0] # There is no need of self assignment
        print(f"FIELDS: {self.helper.sql.get_all_fields()}")
        for index, field in enumerate(self.helper.sql.get_all_fields()):
            variable = StringVar()
            self.insert_variables[field] = variable
            label = LOCALE[field]
            up_index = upperpoints.index(min(upperpoints))
            if field.lower() != "id":
                if "_bool" in field:
                    self.insert_widgets[field] = Checkbutton(self.frame, text=label, variable=variable)
                    if field not in self.helper.sql.get_update_fields():
                        self.insert_widgets[field].configure(state=DISABLED)
                    #self.insert_widgets[field].entry.configure(state="readonly")
                elif "comment" in field:
                    readonly = False
                    if field == "comment":
                        readonly = False
                    self.insert_widgets[field] = LabelText(self.frame, label=label, textvariable=variable, 
                                                           readonly=readonly, context_menu=app.default_context)
                elif "status" in field:
                    if field == "status":
                        name = "PAPERWORK_STATUS"
                        variable.set("GESTIONADO")
                    else:
                        name = field.upper()
                    combo_values = CONFIG.__getattr__(name)
                    self.insert_widgets[field] = LabelCombo(self.frame, name=field, text=LOCALE[field], combo_values=combo_values, 
                                                            textvariable=variable, context_menu=app.default_context)
                    #minsize = (800*0.70)/(len(upperpoints))                
                    #self.insert_widgets[field].grid_columnconfigure(0, minsize=minsize*0.4)
                    #self.insert_widgets[field].grid_columnconfigure(1, minsize=minsize*0.6)
                    #label = Label(self.insert_widgets[field], text=LOCALE[field])
                    #label.grid(row=0, column=0, sticky=(W))
                    #combo = Combobox(self.insert_widgets[field], values=combo_values, textvariable=variable, context_menu=app.default_context)
                    #combo.grid(row=0, column=1, sticky=(E))
                elif "date" in field and field in self.helper.sql.get_update_fields():
                    self.insert_widgets[field] = LabelCalendar(self, label=LOCALE[field], textvariable=variable, context_menu=app.default_context,
                                                               dateformat="%d/%m/%Y")
                else:
                    self.insert_widgets[field] = LabelEntry(self.frame, label=label, textvariable=variable, context_menu=app.default_context)
                    if field not in self.helper.sql.get_update_fields():
                        self.insert_widgets[field].entry.configure(state="readonly")
                if "comment" in field:
                    self.insert_widgets[field].place(relx=0.05 + (up_index * (0.5 / (len(upperpoints) - 1))),
                                                     rely=0.12 + upperpoints[up_index], anchor=W)
                    upperpoints[up_index] += 0.15
                else:
                    self.insert_widgets[field].place(relx=0.05 + (up_index*(0.5/(len(upperpoints)-1))),
                                                     rely=0.08 + upperpoints[up_index], anchor = W)
                    upperpoints[up_index] += 0.07

        #LabelEntry(self.frame, label="Comentario OBO", textvariable=self.obocomment).place(relx=0.55, rely=0.8, anchor = W)
        #LabelText(self.frame, label="Comentario OBO", textvariable=self.obocomment).place(relx=0.55, rely=0.8, anchor = W)
        #Label(self.frame, text = LOCALE["status"]).place(relx=0.05, rely=0.8, anchor = W)
        #self.combo = Combobox(self.frame, state="readonly")
        #self.combo["values"] = CONFIG.PAPERWORK_STATUS
        #self.combo.place(relx=0.23, rely=0.8, anchor = W)
        #self.combo.current(0)
        Button(self, text="Guardar", command=self.save_and_load_new).place(relx=0.3, rely=0.92, anchor = N)
        Button(self, text="Cancelar", command=self.open_list).place(relx=0.7, rely=0.92, anchor = N)
        #Button(self, text="Listado Pendiente", command=self.open_list).place(relx=0.5, rely=0.08, anchor = S)
        #Initializes data
        self.load_data(dato)

    def close_without_save(self):
        if len(self.data) > 0:
            data = {"status": self.last_status, "update_user": self.last_user}
            self.helper.save(self._id,  data)
        self.destroy()

    def load_data(self, _id=0, *, _type="PENDING"):
        order = self.order
        key_item = self.helper.sql._back_correlations[order[0].split(" ")[0]]   
        print(f"Key Item: {key_item}")
        if "date" in key_item:
            if "-" in _id and ":" in _id:
                _id = datetime.datetime.strptime(_id, "%Y-%m-%d %H:%M:%S")
            elif "-" in _id:
                _id = datetime.datetime.strptime(_id, "%Y-%m-%d")
            elif "/" in _id and ":" in _id:
                _id = datetime.datetime.strptime(_id, "%d/%m/%Y %H:%M:%S")
            elif "/" in _id:
                _id = datetime.datetime.strptime(_id, "%d/%m/%Y")
            else:
                _id = datetime.datetime(1970, 1, 1)
        elif "amount" in key_item:
            try:
                _id = float(str(_id).replace(",", "."))
            except (ValueError):
                pass
        else:
            _id = str(_id)
        if key_item.endswith("_bool"):
            if _id == "True":
                _id = True
            else:
                _id = False
            if "asc" in order[0].lower():
                order = [o.replace("asc", "desc") for o in order]
            else:
                order = [o.replace("desc", "asc") for o in order]
        print(_id)
        #if _type == "PENDING":
        #    data = self.helper.load_pending(_id, username=USERNAME.get(), order=order, new=self.new, limit=self.index)
        if self.index < self.helper.len_all_data:
            self._id = self.helper.indexes[self.index]
            for item in self.helper.all_data:
                if item["id"] == self._id:
                    data = item
            self.index += 1
            print(self.order)
            self.check_data = self.helper.load_id(self._id)
            self.last_status = self.check_data["status"]
            self.last_user = self.check_data["update_user"]
            #if self.check_data["status"] in ("PENDIENTE", "BLOQUEADO", "SEGUIMIENTO") and self.check_data["update_user"] in (None, "", USERNAME.get()):
            if ((self.check_data["status"] in ["BLOQUEADO"] and self.check_data["update_user"] in (None, "", USERNAME.get())) or 
                    self.check_data["status"] in ["PENDIENTE", "SEGUIMIENTO"]):
                self.helper.block(self._id, USERNAME.get())
            check_data = self.helper.load_id(self._id)
            if ((self.check_data["status"] in ["BLOQUEADO"] and self.check_data["update_user"] in (None, "", USERNAME.get())) or 
                    self.check_data["status"] in ["PENDIENTE", "SEGUIMIENTO"]):
            #if self._id == check_data[0]["id"]:
                self.data = self.check_data #Así cojo el último registro                
                self.update_field_variable(self.data)
            else:
                self.load_data(check_data["id"]) # Recursividaaaaad
        else:
            print(self.index)
            print(len(self.helper.all_data))
            self.destroy()
        self.new = True

    def on_closing(self):
        self.close_without_save()

    def open_list(self):
        List_Paperwork(self.master, self.papername)
        self.close_without_save()

    def save_and_load_new(self):
        data = {"update_date": datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")}
        for field in self.helper.sql.get_update_fields():
            data[field] = self.insert_variables[field].get()             
        self.helper.save(self._id, data)
        print(self.order)
        field = self.helper.sql._back_correlations[self.order[0].split(" ")[0]]
        self.load_data(self.insert_variables[field].get())

    def update_field_variable(self, data):
        if data:
            for index, field in enumerate(self.helper.sql.get_all_fields()):
                if field in data and data[field]:
                    self.insert_variables[field].set(data[field])
                else:
                    self.insert_variables[field].set("")
            #self.obocomment.set(data["update_comment"])

            #self.status.set(data["status"])
        else:
            for index, field in enumerate(self.helper.fields):
                self.insert_variables[field].set("")


class MainFrame(Frame):
    """
    Main Frame to subclass frames of different services

    """
    def __init__(self, master, service):
        global USERNAME # This is another...
        self.service = service
        Frame.__init__(self, master)
        self.frames = dict()
        self.commitments_frame = tix.Frame(master=self)
        self.customer_historicy_frame = tix.Frame(master=self)
        self.main_frame = tix.Frame(master=self)
        self.main_frame.pack()

        # define the OBO Buttons
        self.OBO_widget = dict()
        self.PAPERS = NONE

        self.message_variable = StringVar()
        self.message_variable.set("If you read this, GOD kills a kitten")
        # now we decorate the main frame
        # label_logoSenpai = Label(self, image=self.logo_senpai)
        # label_logoSenpai.place(relx=0, rely=0, anchor=NW)
        label_logoTranscom = Label(self, image=self.logo_transcom)
        label_logoTranscom.place(relx=1, rely=1, anchor=SE)
        # labelsign = Label(self, text="© 2017-18 Zashel // Robizen for Transcom WW") #Copyright?? Le pongo fechas
        # labelsign.place(relx=1, rely=1, anchor=SE)
        self.helpers = dict()
        self.initialize_main()

    @property
    def main_frame(self):
        return self.frames["main"]

    @main_frame.setter
    def main_frame(self, frame):
        self.frames["main"] = frame

    def OBO_frame(self, papername):
        '''
        To load a PaperworkWindow for OBO functions
        :param papername: Name of the Paper
        :return: None
        '''
        # self.data.update({"insert_date": datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")})
        #PaperworkWindow(self.master, papername)
        List_Paperwork(self.master, papername)
        
    def do_paper_buttons(self):
        print(type(PAPERWORK))
        if self.service in PAPERWORK:
            for item in list(self.OBO_widget.keys()):
                self.OBO_widget[item].grid_forget()
                self.OBO_widget[item].destroy()
                del(self.OBO_widget[item])
            orden = []
            keys = {}
            print(f"PAPERWORK[\"{self.service}\"]: {PAPERWORK[self.service]}")
            for paper in list(PAPERWORK[self.service]):
                papername = paper
                text = LOCALE[papername]
                self.OBO_widget[papername] = Button(self._button_panel, text=text, width=30,
                                                    command=partial(self.OBO_frame, papername))
                self.helpers[papername] = self.get_helper(papername)
                orden.append(text)
                keys[text] = papername
            orden.sort()
            for index, text in enumerate(orden): # With Text It will be weird
                papername = keys[text]
                up_index = index % 2  # use the same way than the paperwork place way to make 2 columns
                if up_index == 0:
                    self.OBO_widget[papername].grid(row=0 + index - up_index, column=up_index, padx=5, pady=5)
                else:
                    self.OBO_widget[papername].grid(row=0 + index - up_index, column=up_index, padx=5, pady=5)

    def initialize_main(self):
        """
        To overrride. This initializes main frame.
        :return: None.
        """
        panel = tkFrame(self.main_frame)  # create a panel for the OBO Widgets
        panel.pack()
        self._button_panel = panel
        '''
        now declare the OBO Widgets in the initialize method, so we can use GRID for the OBO Widgets and Place 
        or pack for the others widgets.
        Grid its so easy compared with place!!
        '''
        self.PAPERS = None
        print(self.service)
        if self.service == "NONE":
            label_logoSenpai = Label(self, image=self.logo_senpai)
            label_logoSenpai.place(relx=0.5, rely=0.1, anchor=N)
            Label(self, text="Bienvenido a Senpai BO Edition\n"
                             "Selecciona la pestaña correspondiente para empezar.\n"
                             "Que tengas una buena jornada ^_^",justify=CENTER).place(relx=0.5, rely=0.5,anchor=N)
            Label(self, text="© 2017-18 Zashel // Robizen for Transcom WW").place(relx=0, rely=1, anchor=SW)
        elif self.service is not None:
            self.PAPERS = PAPERWORK[self.service]
            self.paper_service = f"{self.service}_" # TODO: Cargarme esto
            print(os.path.join(SENPAIPATH, CONFIG.LOGO_SERVICES[self.service]))
            Label(self, image=self.logos[self.service]).place(relx=0, rely=1, anchor=SW)
            Label(self, image=self.logo_senpai).place(relx=0, rely=0, anchor=NW)
            """
            if self.service == "ORANGE":
                from .definitions import ORANGE_PAPERS
                self.PAPERS = ORANGE_PAPERS
                self.paper_service = "ORANGE_"
                label_logoOrange = Label(self, image=self.logo_orange)
                label_logoOrange.place(relx=0, rely=1, anchor=SW)
                label_logoSenpai = Label(self, image=self.logo_senpai)
                label_logoSenpai.place(relx=0, rely=0, anchor=NW)

            elif self.service == "JAZZTEL":
                from .definitions import JAZZTEL_PAPERS
                self.PAPERS = JAZZTEL_PAPERS
                self.paper_service = "JAZZTEL_"
                label_logoJazztel = Label(self, image=self.logo_jazztel)
                label_logoJazztel.place(relx=0, rely=1, anchor=SW)
                label_logoSenpai = Label(self, image=self.logo_senpai)
                label_logoSenpai.place(relx=0, rely=0, anchor=NW)
            """
        self.do_paper_buttons()
        self.totales_variable = StringVar()
        Label(self, textvariable=self.totales_variable).place(relx=0.5, rely=0.98, anchor=CENTER)
        self.check_total()
        #self.check_total_connections()

    def show(self, frame):
        """
        Shows given frame.
        :param frame: Frame to show.
        :return: None.
        """
        if frame in self.frames:
            for f in self.frames:
                self.frames[f].pack_forget()
            self.frames[frame].pack()

    def reinitialize_gandalf(self, *args, **kwargs):
        process = subprocess.Popen(["taskkill", "/f", "/im", "gandalf.exe"])
        process.wait()
        run_gandalf()

    def get_helper(self, papername):
        #papername = papername.split("_")
        #service, paper = papername[0], "_".join(papername[1:])
        return PaperworkHelper(papername)

    @daemonize
    def check_total(self):
        if self.service is not None and self.service != "NONE":
            while True:
                """
                for papername in self.helpers:
                    total = len(self.helpers[papername].load_pending())
                    if total > 0:
                        text = f"{LOCALE[papername]} ({total})"
                    else:
                        text = f"{LOCALE[papername]}"
                    self.OBO_widget[papername]["text"] = text
                """
                data = SENPAI.get_pending()
                for item in data:
                    papername = item["name"]
                    total = item["total"]
                    if total > 0:
                        text = f"{LOCALE[papername]} ({total})"
                    else:
                        text = f"{LOCALE[papername]}"
                    if papername in self.OBO_widget:
                        self.OBO_widget[papername]["text"] = text
                time.sleep(30)

    @daemonize
    def check_total_connections(self):
        from Senpai.senpai import get_connections
        while True:
            self.totales_variable.set(f"{get_connections()} Conexiones")
            time.sleep(2)
    
    #Events!
    def _event_config_file_changed(self, name):
        print(f"Changed {name}")
        if name.lower() == "paperwork":
            time.sleep(2.5)
            self.do_paper_buttons()

'''
class OrangeFrame(MainFrame):
    def __init__(self, master):
        """SQL
        Initializes Orange Frame in a Notebook.
        :param master:
        """
        assert hasattr(master, "main_frame")
        self.main = master.main_frame
        MainFrame.__init__(self, self.main, "ORANGE")
        self.initialize()

    def initialize(self):
        pass
'''

class ServiceFrame(MainFrame):
    def __init__(self, master,service):
        """SQL
        Initializes a Frame for each service in a Notebook.
        :param master:
        """
        print(f" PRUEBA {service}")
        assert hasattr(master, "main_frame")
        self.main = master.main_frame
        MainFrame.__init__(self, self.main, service)
        self.initialize()

    def initialize(self):
        pass

'''
class JazztelFrame(MainFrame):
    def __init__(self, master):
        """
        Initializes Jazztel Frame in a Notebook.
        :param master:
        """
        assert hasattr(master, "main_frame")
        self.main = master.main_frame
        MainFrame.__init__(self, self.main, "JAZZTEL")
'''

class Start_Frame(MainFrame):
    """
    Start Frame for the notebook
    """
    def __init__(self, master):
        """
        Initializes Start Frame in a Notebook.
        :param master:
        """
        assert hasattr(master, "main_frame")
        self.main = master.main_frame
        MainFrame.__init__(self, self.main, "NONE")
        

class PerformanceFrame(MainFrame):
    """
    Performance Frame for user
    """
    def __init__(self, master, user):
        assert hasattr(master, "main_frame")
        self.main = master.main_frame
        MainFrame.__init__(self, self.main, None)
        self._user = user
        self.lock = Lock()
        # Variables
        self.initial_date = StringVar()
        self.final_date = StringVar()

        # Setting
        self.initial_date.set(datetime.datetime.now().strftime("%d/%m/%Y"))
        self.final_date.set(datetime.datetime.now().strftime("%d/%m/%Y"))
        
        Label(self, text=f"Productividad {user}").pack()
        self.columns = CONFIG.PERFORMANCE_BO_COLUMNS
        self.treeview = Treeview(self, columns=self.columns, displaycolumns="#all", show="headings")
        for column in self.columns:
            self.treeview.column(column, width=250)  # Column may be created anyway
            if column in LOCALE:
                text = LOCALE[column]
            else:
                text = column
            self.treeview.heading(column, text=text)
        self.treeview.pack()
        self.update_button = Button(self, text="Actualizar Datos", command=self.update_data)
        self.update_button.pack()
        self.listen_performance()

    def update_data(self, *args):
        with self.lock:
            ini = datetime.datetime.strptime(self.initial_date.get(), "%d/%m/%Y")
            fin = datetime.datetime.strptime(self.final_date.get(), "%d/%m/%Y")+datetime.timedelta(days=1)
            data = SENPAI.get_my_performance(self._user, ini, fin)
            print("performance gotten")
            for i in self.treeview.get_children():
                self.treeview.delete(i)
            print(f"data: {data}")
            if data is not None:
                for service in data:
                    for index, item in enumerate(data[service]):
                        datadict = {"service": service,
                                    "paperwork": item["paperwork"],
                                    "total": item["total"]}
                        self.treeview.insert("", index, values=[column in datadict and LOCALE[datadict[column]] or "" for column in self.columns])

    @daemonize
    def listen_performance(self):
        while True:
            self.update_data()
            time.sleep(300) # Cinco minutos, mejor
    

class App(Frame):
    """
    Main Frame for class.
    """
    INSTANCES = 0
    def __init__(self, master=None):
        """
        Initializes App to given master. Master might be a tix.Tk instance.
        :param master: tix.Tk instance.
        """
        Frame.__init__(self, master)
        
        if App.INSTANCES == 0:
            App.INSTANCES += 1
            self.pack()
            self.initialize()
        else:
            self.destroy()

    def __del__(self):
        App.INSTANCES -= 1
        Frame.__del__(self)

    def initialize(self):
        """
        Initializes App.
        :return:  None
        """
        global USERNAME
        USERNAME = StringVar()
        USERNAME.set(getpass.getuser())
        self.menubar = Menu(self)
        self.menubar.add_command(label = "Notificar Error", command=self.notify_error)
        self.options = Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="Opciones",menu=self.options)
        self.help = Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="Ayuda", menu=self.help)
        self.remote_path = dict()
        self.last_change = dict()
        self.remote_path['SENPAIBO'] = r"//172.19.64.20/Cobros/Assistant/Databases/SenpaiBOInfo"
        self.last_change['SENPAIBO'] = datetime.datetime(2018, 1, 1)
        self.help.add_command(label = "Novedades y Consejos", command=OpenNews.show(self, 'SENPAIBO'))
        ROOT.config(menu = self.menubar)
        self.service =""
        self.main_frame = Frame(self)
        self.notebook = Notebook(self.main_frame, height=450, width=800) # declare and configure the Notebook
        self.notebook.pack()
        self.servicesframes = dict()

        #self.orange_frame = OrangeFrame(self)
        #self.jazztel_frame = JazztelFrame(self)
        self.start_frame = Start_Frame(self)
        self.performance_frame = PerformanceFrame(self, USERNAME.get())
        self.notebook.add(self.start_frame, text="Senpai BO")
        for service in CONFIG["SERVICES"]:
            self.servicesframes[service] = ServiceFrame(self, service)
            self.notebook.add(self.servicesframes[service], text=CONFIG["LOCALE"][service])
        #self.notebook.add(self.jazztel_frame, text="Jazztel")
        self.notebook.add(self.performance_frame, text="Productividad")
        self.notebook.bind("<<NotebookTabChanged>>", self.tab_changed)
        self.main_frame.pack()

        user = USERNAME.get()
        self.remote_path['user'] = r"//172.19.64.20/Cobros/Assistant/Databases/Users/" + user.lower()
        self.last_change['user'] = datetime.datetime.now()
        self.last_change['user'] = self.last_change['user'].replace(hour=0, minute=0, second=0,
                                                                    microsecond=0)
        if not os.path.exists(self.remote_path['user']):
            os.makedirs(self.remote_path['user'])
        self.notebook.pack()
        self.default_context = ContextualMenu(self)
        Thread(target=self.newsDaemon, daemon=True).start()
        self.main_frame.lift()

    def tab_changed(self,event):
        print(f"Tab changed: {self.notebook.select()}")
        print(self.notebook.tab(self.notebook.select())['text'])
        if "range" in self.notebook.tab(self.notebook.select())['text']:
            self.last_change['Orange'] = datetime.datetime.now()
            self.last_change['Orange'] = self.last_change['Orange'].replace(hour=0, minute=0, second=0, microsecond=0)
            self.remote_path['Orange'] = r"//172.19.64.20/Cobros/Assistant/Databases/SenpaiBOInfo/Orange"
        if "Jazztel" in self.notebook.tab(self.notebook.select())['text']:
            self.last_change['Jazztel'] = datetime.datetime.now()
            self.last_change['Jazztel'] = self.last_change['Jazztel'].replace(hour=0, minute=0, second=0, microsecond=0)
            self.remote_path['Jazztel'] = r"//172.19.64.20/Cobros/Assistant/Databases/SenpaiBOInfo/Jazztel"


    def notify_error(self):
        NotifyErrorWindow(self.master, "SenpaiBO")

    def newsDaemon(self):
        while True:
            for directory in self.remote_path.copy():
                if len(os.listdir(self.remote_path[directory]))>0:
                    last_remote_change = datetime.datetime.fromtimestamp(os.path.getmtime(self.remote_path[directory]))
                    #print(f"directorio: {directory}, hora: {last_remote_change}")
                    if last_remote_change > self.last_change[directory]:
                        OpenNews.show(self, directory)
            time.sleep(5)
    
    #Events!
    def _event_config_file_changed(self, name):
        global LOCALE
        if name.lower() == "config":
            from definitions import Locale
            LOCALE = Locale(CONFIG["LOCALE"])

            
class OpenNews(App):
    def __init__(self):
        pass

    def show(self, directory):
        try:
            news = SuperInfoFrame(self, "Mensajes", "SenpaiBO: Avisos y novedades", self.remote_path[directory])
        except:
            pass
        else:
            news.lift()
        finally:
            self.last_change[directory] = datetime.datetime.fromtimestamp(os.path.getmtime(self.remote_path[directory]))        


class Splash(Frame):
    """
    Splash Screen for initializing at loading.
    """
    def __init__(self, master):

        Frame.__init__(self, master=master)
        #self.splash = Label(self, text="Esto debería ser un Splash Screen para ir tirando.")
        #self.splash.pack()
        self.pack()
        self.initialized = False
        SplashInEvent()

    def _event_splash_in(self):
        global json, time, CONFIG, DEBUG, partial, SenpaiBOAPI, Thread, app, \
            SENPAI, LOCALE, LoggedInEvent, SplashInEvent, SplashOutEvent, root, Papers, \
            PAPERWORK, PaperworkHelper, DEBUG_FLAG, SQL, \
            copy, paste, gc
        # Create components of splash screen.
        if self.initialized is False:
            window = tix.Toplevel(self.master)
            canvas = tix.Canvas(window)
            file = os.path.join(SENPAIPATH, "splash.gif") # Y es una ruta relativamente absoluta
            imagesplash = tix.PhotoImage(master=window, file=file)
            # Get the screen's width and height.
            scrW = window.winfo_screenwidth()
            scrH = window.winfo_screenheight()
            # Get the images's width and height.
            imgW = imagesplash.width()
            imgH = imagesplash.height()
            # Compute positioning for splash screen.
            Xpos = (scrW - imgW) // 2
            Ypos = (scrH - imgH) // 2
            # Configure the window showing the logo.
            window.overrideredirect(True)
            window.geometry(f"+{Xpos}+{Ypos}")
            # Setup canvas on which image is drawn.
            canvas.configure(width=imgW, height=imgH, highlightthickness=0)
            canvas.grid()
            # Show the splash screen on the monitor.
            canvas.create_image(imgW // 2, imgH // 2, image=imagesplash)
            f = tix.Frame(window, background="#FFF")
            f.place(relx=0.5, rely=1, anchor=S)
            splash = Label(f, text="Senpai 0.1", background="#FFF")
            splash.pack()
            bar = tix.Meter(f, fillcolor="#000", background="#FFF", borderwidth=0)
            bar.pack()
            window.update()
            window.lift()

            splash["text"] = "Importando cosas"
            import json
            from zashel.utils import copy, paste
            bar["value"] = 0.1
            splash["text"] = "Importando time"
            import time
            bar["value"] = 0.2
            splash["text"] = "Importando CONFIG y PAPERWORK"
            from Senpai.definitions import CONFIG, DEBUG_FLAG, PAPERWORK, SQL
            bar["value"] = 0.3
            splash["text"] = "Importando partial"
            from functools import partial
            import gc
            bar["value"] = 0.4
            splash["text"] = "Importando SenpaiAPI"
            from SenpaiBO.senpaiBO import SenpaiBOAPI, Papers, PaperworkHelper
            from Senpai.senpai import DEBUG
            bar["value"] = 0.5
            splash["text"] = "Importando Thread"
            from threading import Thread
            bar["value"] = 0.6
            splash["text"] = "Instanciando SenpaiAPI"
            SENPAI = SenpaiBOAPI()
            bar["value"] = 0.7
            splash["text"] = "Inicializando SenpaiAPI"
            SENPAI.initialize()
            bar["value"] = 0.8
            splash["text"] = "Definiendo traducciones para campos de tablas"
            from Senpai.definitions import LOCALE
            bar["value"] = 0.9
            splash["text"] = "Instanciando Aplicación"
            try:
                app = App(ROOT)
                bar["value"] = 1
                splash["text"] = "Iniciando aplicación"
            except Exception as e:
                print(e)
                raise
            canvas.destroy()
            splash.destroy()
            bar.destroy()
            #self.destroy()
            window.destroy()
            window.quit() # This exists the mainloop
            self.initialized = True
            SplashOutEvent()

    def _event_splash_out(self):
        pass


ROOT.iconbitmap(r"c:\Program Files (x86)\Assistant\SenpaiBO.ico")
print("let's execute")
splash = Splash(ROOT)
print("splash instantiated")
ROOT.withdraw() # Ocultamos ROOT
print("root withdrawn")
splash.mainloop()
print("mainlooping")
time.sleep(1)
ROOT.update() # Actualizamos ROOT
ROOT.deiconify() # Devolvemos ROOT a la normalidad
app.mainloop()
SENPAI.exit()

