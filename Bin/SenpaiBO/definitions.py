import os
import pythoncom
from Mordor import Council
from MordorManager import get_api
from zashel.gapi import SCOPE
from zashel.transcom import TranscomAPI

'''
SCOPES = [SCOPE.DRIVE, SCOPE.SPREADSHEETS]

with open("secret.txt") as t:
    secret = t.read()

def get_senpai(): #Deprecated
    """
    Gets the API for Google and logs in
    :return: the API
    """
    pythoncom.CoInitialize()
    _api = TranscomAPI(scopes=SCOPES,
                       secret_data={"installed": {
                           "client_id": "137432867660-g65uia96vl61olij75crum7qh0dl9c8b.apps.googleusercontent.com",
                           "project_id": "assistant-updater", "auth_uri": "https://accounts.google.com/o/oauth2/auth",
                           "token_uri": "https://accounts.google.com/o/oauth2/token",
                           "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
                           "client_secret": secret,
                           "redirect_uris": ["urn:ietf:wg:oauth:2.0:oob",
                                             "http://localhost"]}})
    _api.login()
    _api.teamdrive_open("Senpai")
    return _api

def get_historicy(): #Deprecated
    """
    Gets the API for Google and logs in
    :return: the API
    """
    pythoncom.CoInitialize()
    _api = TranscomAPI(scopes=SCOPES,
                       secret_data={"installed": {
                           "client_id": "137432867660-g65uia96vl61olij75crum7qh0dl9c8b.apps.googleusercontent.com",
                           "project_id": "assistant-updater", "auth_uri": "https://accounts.google.com/o/oauth2/auth",
                           "token_uri": "https://accounts.google.com/o/oauth2/token",
                           "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
                           "client_secret": secret,
                           "redirect_uris": ["urn:ietf:wg:oauth:2.0:oob",
                                             "http://localhost"]}})
    _api.login()
    _api.teamdrive_open("Historicy")
    return _api
'''
get_senpai = get_api
get_historicy = get_api


SENPAI = get_senpai()
HISTORICY = get_historicy()
PATH = os.path.join(os.environ["APPDATA"], "Senpai")
REMOTE_PATH = r"//172.19.64.20/cobros/O.B.O/Senpai/Config"
if not os.path.exists(PATH):
    os.makedirs(PATH)
CONFIG = Council(None, name="Config", path=PATH, remote_path=REMOTE_PATH)
PAPERWORK = Council(None, name="Paperwork", path=PATH, remote_path=REMOTE_PATH)
SQL = Council(None, name="Sql", path=PATH, remote_path=REMOTE_PATH)

LOCALPATH = os.path.join(os.environ["LOCALAPPDATA"], "senpai_BO")
DEBUG = True

PROGRAMDATA = os.path.join(os.environ["PROGRAMDATA"], "SenpaiBO")
if not os.path.exists(PROGRAMDATA):
    os.makedirs(PROGRAMDATA)

