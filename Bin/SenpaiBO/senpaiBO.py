try:
    import senpai_BO
    __package__ = "SenpaiBO"
except ImportError:
    pass
import base64
import comtypes, comtypes.client
import datetime
import difflib
import json
import os
import re
import shelve
import string
import time
from collections import OrderedDict
from Senpai.definitions import SENPAI, HISTORICY, CONFIG, LOCALPATH, SQL, LOCALE, PAPERWORK
from Senpai.senpai import Paperwork
from decimal import Decimal
from events import *
from threading import Thread
from Mordor import TheOneRing, Council, FellowshipOfTheRing, AlreadyServing, Lembas, LembasList, FlyYouFools
from adohelper import Where, sql


Papers = {}
for item in SQL:
    if item.endswith("_PAPER"):
        Papers[item] = Paperwork(item)


def sql_direct(conn_string, sql, headers=None):
    import comtypes
    print("Doing query")
    comtypes.CoInitialize()
    conn = comtypes.client.CreateObject("ADODB.Connection")
    conn.Mode = 3 # Read/Write
    cursor = comtypes.client.CreateObject("ADODB.RecordSet")
    conn.Open(conn_string)
    print("Open cursor")
    try:
        cursor.Open(sql, conn)
    except comtypes.COMError:
        print("ERROR")
        print(conn_string)
        print(sql)
        print("+"*20)
        raise
    final = list()
    if cursor.EOF is True or cursor.BOF is True:
        print("EOF")
        return final
    cursor.MoveFirst()
    print("Executing query")
    if headers is None:
        headers = [cursor.Fields.Item(index).Name for index in range(cursor.Fields.Count)]
    while True:
        if cursor.EOF is False and cursor.BOF is False:        
            final.append(OrderedDict([(head, cursor.Fields.Item(index).Value) for index, head in enumerate(headers)]))
            cursor.MoveNext() # Se queda aquí pillado?
        else:
            break
    cursor.Close()
    conn.Close()
    return final


class PaperworkHelper:
    def __init__(self, paper):
        #print("done")
        self.paper = paper
        self.sql = Paperwork(paper)
        self.fields = self.sql.get_insert_fields()
        self.update_fields = self.sql.get_update_fields()
        self.indexes = list()
        self.len_all_data = int()
        self.where = None

    def block(self, _id, username=os.environ["USERNAME"]):
        self.save(_id, {"status": "BLOQUEADO", "update_user": username})

    def load(self, where, order=["Id asc"], limit=None, fields=None):
        kwargs = dict(order=order, limit=limit)
        if fields is not None:
            kwargs["fields"] = fields
        return self.sql.get(where, **kwargs)

    def load_pending(self, _id=0, *, username=os.environ["USERNAME"], order=["Id asc"], new=False, limit=0, count=False):
        field, method = order[0].split(" ")
        if method == "desc":
            if new is True:
                method = "<"
            else:
                method = "<="
        else:
            if new is True:
                method = ">"
            else:
                method = ">="
        if field == "Id":
            _id = int(_id)
        if _id is None:
            if "date" in field:
                _id = datetime.datetime(1970, 1, 1)
            if "amount" in field:
                _id = 0
            else:
                _id = "a"
        if isinstance(_id, bool):
            wherezero = None
        else:
            wherezero = Where((field, method, _id))
            wherezero.Or(field, "is", "null")
        wherezero = None # Uhm...
        whereone = Where(("status", "=", "PENDIENTE"))
        #wheresone = Where(("update_user", "=", ""))
        #wheresone.Or("update_user", "is", "null")
        #wheresone.Or("update_user", "=", username)
        #whereone.And(wheresone)
        wheretwo = Where(("status", "=", "BLOQUEADO"))
        wheretwo.And("update_user", "=", username)
        whereone.Or(wheretwo)
        wherethree = Where(("status", "=", "SEGUIMIENTO"))
        #wherethree.And("update_user", "=", username) # Son muy torpes y dejan en seguimiento gilipolleces
        whereone.Or(wherethree)
        if wherezero is not None:
            where = Where(wherezero)
            where.And(whereone)
        else:
            where = whereone
        if isinstance(self.where, Where):
            final_where = Where(where)
            final_where.And(self.where)
            where = final_where
        print(f"Final_WHERE {sql(where)}")
        #if "urgent_bool" in self.fields:
        #    where.And("urgent_bool", "=", True)
        m = order[0].split(" ")[1]
        if "id " not in order[0].lower():
            order.append(f"Id {m}")
        kwargs = dict(order=order, limit=0)
        if count is True:
            kwargs["fields"] = ["count(Id) as total"]
        all_data = self.load(where, **kwargs)
        if len(all_data) > 0:
            self.initialize_data(all_data, all_data[0].columns)
        print(f"Total indexes {len(self.indexes)}")
        return all_data

    def initialize_data(self, all_data):
        print(type(all_data))
        self.all_data = [dict([(key in self.sql._correlations and self.sql._correlations[key] or key.lower(), all_data[item]) 
                               for key in all_data[item]]) for item in all_data]
        self.len_all_data = len(all_data)
        self.indexes = [item["id"] for item in self.all_data]

    def load_id(self, _id):
        where = Where(("id", "=", _id)) 
        data = self.sql.get(where)
        return data[0] #Se había roto

    def save(self, _id, data):
        where = Where(("Id", "=", _id))
        where.And("status", "in", ("PENDIENTE", "BLOQUEADO", "SEGUIMIENTO"))
        #for item in list(data.keys()):
        #    if "date" in item and not data[item]:
        #        del(data[item])
        self.sql.put(where, data)
        time.sleep(0.75)
           

class SenpaiBOAPI(FirstLightOnTheFifthDay):
    """
    API for senpai to implement in forward GUI
    """
    GOOGLE = SENPAI # Google api to spreadsheets and drive
    CONFIG = CONFIG # Configuration File
    #PAPERBACK = PAPERBACK # Paperback file

    def __init__(self):
        """
        Initializes the API
        """
        print("LOADING SenpaiAPI")
        #TheOneRing.__init__(self, "SenpaiBO", manager_port=5506)
        print("Done TheOneRing")
        FirstLightOnTheFifthDay.__init__(self)
        print("Done FirstLightOnTheFifthDay")
        # Variables
        import getpass
        self.username = getpass.getuser()

    def exit(self):
        """
        It necesarily might exit cleanly.
        :return: None.
        """
        pass

    def get_config(self):
        """
        Getter to CONFIG to use with managers
        :return: API.CONFIG
        """
        return self.CONFIG

    def get_google(self):
        """
        Getter to GOOGLE to use with managers
        :return: API.GOOGLE
        """
        return self.GOOGLE

    def get_paperback(self):
        """
        Getter to PAPERBACK to use with managers
        :return: API.PAPERBACK
        """
        return self.PAPERBACK

    def initialize(self):
        """
        Initializes SenpaiAPI.
        :return: None
        """
        # print("INITIALIZE SENPAI")
        # self._phone_history = Persistency("phone_history", ["number", "time"])
        # self._customer_history = Persistency("customer_history", ["number", "customer_id", "external_id"])

    def get_paperwork(self, service, paper):
        """
        Returns a PaperworkHelper object
        """
        return PaperworkHelper(service, paper)

    def get_pending(self):
        sql_list = dict()
        sql = "select '{name}' as name, count(Id) as total, min(FECHA) as antiguedad from [{table}] where GESTIONADO = 'PENDIENTE'"
        conns = {}
        for service in CONFIG["SERVICES"]:
            for paper in PAPERWORK[service]:
                if paper not in Papers and paper.endswith("_PAPER"):
                    Paper[paper] = Paperwork(paper)
                if paper in Papers:
                    pwork = Papers[paper]
                    conn = pwork.conn
                    ds = pwork._definitions["data_source"]
                    conns[ds] = conn
                    if ds not in sql_list:
                        sql_list[ds] = []
                    sql_list[ds].append(sql.format(name=paper, table=pwork.table))
        final = []
        for ds in sql_list:
            final_sql = " union ".join(sql_list[ds])
            conn = conns[ds]
            final.extend(sql_direct(conn, final_sql))
        return final

    def get_my_performance(self, who, ini_date, end_date):
        sql_list = dict()
        sql = "select '{name}' as paperwork, count(Id) as total from " \
              "(select Id, FECHA_GESTION, {user_field}, GESTIONADO from [{table}]" \
              " where FECHA_GESTION >= #{ini}# and FECHA_GESTION < #{fin}# and GESTIONADO in ('GESTIONADO', 'CERRADO') and " \
              " {user_field} = '{user}' order by FECHA_GESTION, {user_field}, GESTIONADO) group by '{name}'"
        sql_list = dict()
        ini = ini_date.strftime("%Y-%m-%d")
        fin = end_date.strftime("%Y-%m-%d")
        final = {}
        conns = {}
        done_papers = list()
        for service in CONFIG["SERVICES"]:
            for paper in PAPERWORK[service]:
                if paper not in Papers and paper.endswith("_PAPER"):
                    Paper[paper] = Paperwork(paper)
                if paper in Papers and paper not in done_papers:
                    pwork = Papers[paper]
                    user_field = pwork.update_user
                    conn = pwork.conn
                    ds = pwork._definitions["data_source"]
                    conns[ds] = conn
                    if (ds, service) not in sql_list:
                        sql_list[(ds, service)] = list()
                    sql_list[(ds, service)].append(sql.format(name=paper, table=pwork.table, ini=ini, fin=fin, user=who, user_field=user_field))
                    done_papers.append(paper)
        for ds, service in sql_list:
            print(ds, service)
            conn = conns[ds]
            from math import ceil
            if service not in final:
                final[service] = []
            for x in range(ceil(len(sql_list[(ds, service)])/20)):
                final_sql = " union ".join(sql_list[(ds, service)][x*20: (x+1)*20])
                #print("*"*20)
                #print(conn)
                #print(final_sql)
                #print("*"*20)
                final[service].extend(sql_direct(conn, final_sql))
        print(final)
        return final
            
            
                
        
