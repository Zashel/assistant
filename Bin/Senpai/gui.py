import os
import sys
import stat
for p in sys.path:
    os.chmod(p, stat.S_IWRITE)
from tkinter import *
from tkinter.ttk import *
import win32api
from zashel.utils import daemonize
tkButton = Button
from tkinter import tix
from events import FirstLightOnTheFifthDay
from events import Event as Balrog # There is already an "Event" in tkinter.
from events import _run as run_gandalf
from tkutils import *

TITLE = "Senpai"

SENPAIPATH = os.path.dirname(os.path.abspath(__file__)) # A present for you
LoggedInEvent = Balrog("logged_in")
SplashInEvent = Balrog("splash_in")
SplashOutEvent = Balrog("splash_out") # _event_splash_out
CommitmentChangedEvent = Balrog("commitment_changed")

ROOT = tix.Tk()
#This configures the graphical interface
ROOT.title(TITLE)  #Name of the window

#This is done this way to show a Splash screen while loading.
tkFrame = Frame
class Frame(tix.Frame, FirstLightOnTheFifthDay):
    """
    Base frame to listen Gandalf Events.
    """
    def __init__(self, master):
        tix.Frame.__init__(self, master=master, **APPEARANCE)
        FirstLightOnTheFifthDay.__init__(self, True)
        file_senpailogo = os.path.join(SENPAIPATH, "senpailogo.gif")  # Y es una ruta relativamente absoluta
        self.logo_senpai = tix.PhotoImage(file=os.path.join(SENPAIPATH, "senpailogo.gif"))
        self.logo_transcom = tix.PhotoImage(file=os.path.join(SENPAIPATH, "transcomlogo.gif"))
        #self.logos = {}
        #for service in CONFIG.LOGO_SERVICES:
        #    self.logos[service] = tix.PhotoImage(file=os.path.join(SENPAIPATH, CONFIG.LOGO_SERVICES[service]))
        
    def destroy(self):
        FirstLightOnTheFifthDay.do_out(self)
        tix.Frame.destroy(self)
        

tkWindow = Window
class Window(tkWindow, FirstLightOnTheFifthDay):
    """
    Windows class for nonmodal widows over root.
    """
    def __init__(self, master, *, title=None, data=None):     
        tkWindow.__init__(self, master, title=title, data=data)
        FirstLightOnTheFifthDay.__init__(self)
        
    def destroy(self):
        FirstLightOnTheFifthDay.do_out(self)
        tkWindow.destroy(self)

        
class NotifyErrorWindow(Window):
    '''
    Window to show a entry to notify an error to developers
    '''
    def __init__(self, master, program="Senpai"):
        '''
        Initializes the commitments Altitude windows advice
        :param master:
        '''
        Window.__init__(self, master,title="Notificar Error")
        self.errorvar = StringVar()
        self.frame = tkFrame(self)
        self.frame.pack()
        self.frame.config(height=200, width=400)
        self.program = program
        description = LabelText(self.frame, label="Describe el error:", textvariable=self.errorvar)
        description.place(relx=0.5, rely=0.3, anchor=N)
        Button(self.frame, text="Enviar notificación del error",command = partial(self.send_notify)).place(relx=0.5, rely=0.8, anchor=N)

    def send_notify(self):
        if self.errorvar.get() == "":
            win32api.MessageBox(0, 'Describe el error para poder solucionarlo cuanto antes.')
        else:
            data = {"insert_user": f"{os.environ['USERNAME']}@{os.environ['COMPUTERNAME']}",
                    "insert_date": datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
                    "status": "PENDIENTE",
                    "program": self.program,
                    "comment": self.errorvar.get()}
            DEBUG.post(data)
            self.destroy()


class CommitmentsAltitudeWindow(tkWindow):
    '''
    Window to advice that the commitments must be saved in altitude
    '''
    def __init__(self, master):
        '''
        Initializes the commitments Altitude windows advice
        :param master:
        '''
        tkWindow.__init__(self, master, title="Cliente cargado en Altitude")
        Label(self, text="El cliente se encuentra cargado en Altitude").pack()
        Label(self, text="Tipifica el compromiso cuando finalices la llamada").pack()
        Label(self, text="no es necesario ninguna gestión adicional").pack()
        Label(self, text="Gracias por su colaboración").pack()
        Label(self, text="Vuelva pronto...").pack()


class CommitmentPopUp(Window):
    """
    Window to check and operate with created commitments
    """
    def __init__(self, master, commitment):
        Window.__init__(self, master, title="Compromiso de pago")
        self.helper = COMMITMENTS_HELPER
        self.vars = dict()
        self.insert_widgets = dict()
        self.in_db = commitment["in_db"]
        self.easycode = int(commitment["code"])
        self.old_recall = commitment["recall_date"]
        self.ids = {"_treeview_id": commitment["_treeview_id"], "_commitment_id": commitment["_commitment_id"]}
        self.fullname = StringVar()
        self.users = None
        self.search = StringVar()
        for item in CONFIG.COMMITMENTS_WINDOW:
            self.vars[item] = StringVar()
            if item in commitment:
                self.vars[item].set(commitment[item])
            else:
                if item == "insert_user":
                    self.vars[item].set(SENPAI.is_logged()) # We may change this
                elif item == "insert_date":
                    self.vars[item].set(datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
        upperpoints = [0, 0]
        for index, field in enumerate(CONFIG.COMMITMENTS_WINDOW):
            up_index = index % len(upperpoints)
            # disable the labelcalendar cause its freeze the window
            if "date" in field and field != "insert_date":
                self.insert_widgets[field] = LabelCalendar(self, label=LOCALE[field], textvariable=self.vars[field],
                                                           context_menu=app.default_context, dateformat="%d/%m/%Y")
            elif field == "status":
                self.insert_widgets[field] = LabelCombo(self, name=field, text=LOCALE[field], combo_values=CONFIG.COMMITMENTS_STATUS, 
                                                        textvariable=self.vars[field], context_menu=app.default_context)
            elif field == "assigned":
                self.insert_widgets[field] = tkFrame(self)
                LabelEntry(self, label=LOCALE[field], textvariable=self.fullname, context_menu=app.default_context).place(
                           relx=0.05+(up_index*(0.5/(len(upperpoints)-1))),
                           rely=0.1 + upperpoints[up_index], anchor = W)
                upperpoints[up_index] += 0.1
                up_index = (index+1) % len(upperpoints)
                search = LabelEntry(self, label=LOCALE["search"], textvariable=self.search, context_menu=app.default_context)
                search.place(relx=0.05+(up_index*(0.5/(len(upperpoints)-1))), rely=0.1 + upperpoints[up_index], anchor = W)
                search.entry.bind("<Return>", self.do_search)
                columns = CONFIG.USERS_COLUMNS
                self.users_tree = Treeview(self.insert_widgets[field], columns=columns, displaycolumns="#all", show="headings", height=4)
                for column in columns:
                    self.users_tree.column(column, width=120)
                    self.users_tree.heading(column, text=LOCALE[column])
                if self.vars[field].get():
                    self.search_for_user(usr_name=self.vars[field].get())
                    while self.users is None:
                        time.sleep(0.25) #Let's wait
                    if self.users:
                        self.fullname.set(self.users[0]["fullname"]) # Take that you fool!
                self.users_tree.bind("<Double-1>", self.set_user)  
                self.users_tree.pack() # Copy-Paste as lifetyle
                
            else:
                self.insert_widgets[field] = LabelEntry(self, label=LOCALE[field], textvariable=self.vars[field], context_menu=app.default_context)
            if field != "assigned":
                self.insert_widgets[field].place(relx=0.05+(up_index*(0.5/(len(upperpoints)-1))),
                                                 rely=0.1 + upperpoints[up_index], anchor = W)
                                             
            else:
                self.insert_widgets[field].place(relx=0.50, rely=0.15+max(upperpoints), anchor = CENTER)
            upperpoints[up_index] += 0.1
            

        self.insert_widgets["insert_user"].entry.configure(state=DISABLED) # to disable the entry.
        self.insert_widgets["insert_date"].entry.configure(state=DISABLED)  # to disable the entry.

        Button(self, text="Guardar y Cerrar", command=self.save).place(relx=0.3, rely=0.9, anchor = N)
        Button(self, text="Cerrar", command=self.close).place(relx=0.7, rely=0.9, anchor = N)
        self.config(height=600, width=800, bd=2)

    def close(self, event=None):
        """
        Just closes the window
        """
        self.destroy()

    def do_search(self, event):
        data = self.search.get()
        data = data.strip()
        if data.startswith("ops"):
            print("searching by ops")
            self.search_for_user(usr_name=data)
        elif data.isnumeric():
            print("searching by login")
            self.search_for_user(login=data)
        else:
            print("searching by fullname")
            self.search_for_user(fullname=data)

    def search_for_user(self, *, fullname=None, usr_name=None, login=None, only_loggedin=False):
        """
        Searches for list of users with given criteria
        """
        SENPAI.get_users(fullname=fullname, usr_name=usr_name, login=login, only_loggedin=only_loggedin)

    def set_user(self, event):
        item = self.users_tree.selection()[0]
        self.vars["assigned"].set(self.users_tree.item(item)["values"][CONFIG.USERS_COLUMNS.index("usr_name")])
        self.fullname.set(self.users_tree.item(item)["values"][CONFIG.USERS_COLUMNS.index("fullname")])

    def save(self, event=None):
        """
        Saves data to database
        """
        data = {"assigned": self.vars["assigned"].get(),
                "comment": self.vars["comment"].get(), 
                "status": self.vars["status"].get()}
        if self.old_recall != self.vars["recall_date"].get():
            data["recall_date"] = self.vars["recall_date"].get()
        if self.in_db is True:
            where = Where(("easycode", "=", self.easycode))
            where.And("insert_user", "=", self.vars["insert_user"].get())
            self.helper.put(where, data)
        else:
            data.update({"easycode": self.easycode,
                         "insert_user": self.vars["insert_user"].get(),
                         "insert_date": self.vars["insert_date"].get()})
            print(data)
            self.helper.post(data)
        data.update(self.ids)
        CommitmentChangedEvent(data)
        self.close()

    def _event_got_users(self, users):
        print("Recibido evento")
        print([user for user in users])
        self.users = users
        columns = ["usr_name", "fullname", "position_id", "logged_as_agent"]
        for i in self.users_tree.get_children():
            self.users_tree.delete(i)
        for index, data in enumerate(self.users):
            self.users_tree.insert("", index, values=[column in data and str(data[column]) or "" for column in columns])


class QuestionWindow(tkWindow):
    """
    Window to Question Frame
    """
    def __init__(self, master, papername, data):
        """
        Initializes the question window
        :param master: Master for Window
        :param papername: name of the Paper
        :param data: Data assigned to paperwork
        """
        self.data = data
        self.papername = papername
        print("QUESTION: " + self.papername)
        if papername in LOCALE:
            paper = LOCALE[papername]
        else:
            paper = papername
        print(paper)
        tkWindow.__init__(self, master, data=data, title=f"Gestion \"{paper}\" Detectada")
        Label(self,text= f"Gestion BO Detectada:").pack()
        Label(self,text= f"¿Quieres almacenar este {paper}?").pack()
        Button(self, text="Guardar", command=self.save_and_close).pack()
        Button(self, text="Cancelar", command=self.close_without_save).pack()

    def save_and_close(self):
        print(self.papername)
        PaperworkWindow(self.master, self.papername, data=self.data)
        self.destroy()

    def close_without_save(self):
        self.destroy()
        

class PaperworkWindow(Window):
    """
    Window to paperwork
    """
    def __init__(self, master, paperwork, data=None, *, action="INSERT"):
        """
        Initializes new Paperwork window
        :param master: Master for Window
        :param paperwork: Name of Paperwork
        :param data: Data assigned to paperwork
        """
        text = LOCALE[paperwork]
        Window.__init__(self, master, data=data, title=text)
        self.regex_keys = []
        if "regex" in SQL[paperwork]:
            for regex in SQL[paperwork]["regex"]:
                for item in SQL[paperwork]["regex"][regex]:
                    if "regex" in item:
                        self.regex_keys.append(item["regex"])
        data = self.parse_data(data, paperwork)
        self.papername = paperwork
        print("*"*20)
        print(paperwork)
        print("*"*20)
        self.fields = list()
        self.service, self.paper = paperwork[:paperwork.index("_")], paperwork[paperwork.index("_")+1:]
        try:
            service = list(filter(lambda x: paperwork in PAPERWORK[x], PAPERWORK))
            service = len(service) > 0 and service[0] or ""
            if service:
                data["segment"] = list(filter(lambda x: x in CONFIG["SERVICES"][service], SENPAI._open_campaigns))
                data["segment"].sort() # Reverse Sort for priorice outbond over inbound. <-- TESTING!!!
                data["segment"] = len(data["segment"]) > 0 and data["segment"][-1] or ""                
        except Exception as e:
            print(e)
        #print(self.service)
        #print(self.paper)
        self.sql = Papers[self.papername]
        if action == "INSERT":
            self.fields = self.sql.get_insert_fields()

        # Widgets
        self.insert_widgets = dict()
        self.insert_variables = dict()
        self.frame = tkFrame(self)
        self.frame.pack()
        self.frame.config(height=500, width=800)

        self.customername = "name" in data and data["name"] or ""
        self.customerlabel = Label(self.frame, text=self.customername)
        self.customerlabel.place(relx=0.5, rely=0, anchor = N)
        upperpoints = [0, 0] # There is no need of self assignment
        
        for index, field in enumerate(self.fields):
            variable = StringVar()
            self.insert_variables[field] = variable
            label = LOCALE[field]
            up_index = upperpoints.index(min(upperpoints))
            if "date" in field and field != "insert_date":
                self.insert_widgets[field] = LabelCalendar(self.frame, label=label, textvariable=variable, context_menu=app.default_context,
                                                           dateformat="%d/%m/%Y")
            elif "_bool" in field:
                self.insert_widgets[field] = Checkbutton(self.frame, text=label, variable=variable)
            elif "_combo" in field:
                self.insert_widgets[field] = LabelCombo(self.frame, text=label, name=field, textvariable=variable, 
                                                        combo_values=CONFIG.__getattr__(field.upper()), context_menu=app.default_context)
            elif field == "segment":
                self.insert_widgets[field] = LabelCombo(self.frame, text=label, name=field, textvariable=variable, 
                                                        combo_values=CONFIG["SERVICES"][service],
                                                        context_menu=app.default_context)
            elif "comment" in field:
                self.insert_widgets[field] = LabelText(self.frame, label=label, textvariable=variable, context_menu=app.default_context)
            else:
                self.insert_widgets[field] = LabelEntry(self.frame, label=label, textvariable=variable, context_menu=app.default_context)
            if field in data:
                if "date" in field and isinstance(data[field], str):
                    splitter = data[field].split("/")
                    if len(splitter[-1]) == 2:
                        splitter[-1] = "20"+splitter[-1]
                        data[field] = "/".join(splitter)
                self.insert_variables[field].set(data[field])
            elif field == "invoice_date" and (field not in data or not data[field] or data[field]==0):
                self.insert_variables[field].set("01/01/2000")
            elif field == "payment_date" and (field not in data or not data[field]):
                self.insert_variables[field].set(datetime.datetime.now().strftime("%d/%m/%Y"))
            if self.insert_variables[field].get()=="":
                self.insert_variables[field].set("0")
            if "comment" in field:
                self.insert_widgets[field].place(relx=0.05 + (up_index * (0.5 / (len(upperpoints) - 1))),
                                                 rely=0.19 + upperpoints[up_index], anchor=W)
                upperpoints[up_index] += 0.20
            else:
                self.insert_widgets[field].place(relx=0.05 + (up_index*(0.5/(len(upperpoints)-1))),
                                                 rely=0.15 + upperpoints[up_index], anchor = W)
                upperpoints[up_index] += 0.1

        self.insert_widgets["insert_user"].entry.configure(state=DISABLED) # to disable the entry.
        self.insert_widgets["insert_date"].entry.configure(state=DISABLED)  # to disable the entry.
        self.insert_widgets["status"].entry.configure(state=DISABLED)  # to disable the entry.
        #self.insert_widgets["segment"].entry.configure(state=DISABLED)
        self.insert_variables["status"].set("PENDIENTE") # Value by default

        Button(self, text="Guardar", command=self.check_date).place(relx=0.3, rely=0.9, anchor = N)
        Button(self, text="Cancelar", command=self.close_without_save).place(relx=0.7, rely=0.9, anchor = N)

    def check_date(self):
        '''
        Checks the date fields for the correct format of the date
        :return: None
        '''
        checked = True
        for field in self.fields:
            if "date" in field:
                if field == "insert_date":
                    pass
                else:
                    try:
                        datetime.datetime.strptime(self.insert_variables[field].get(), "%d/%m/%Y")
                    except(ValueError):
                        win32api.MessageBox(0, 'Revisa el formato de la fecha introducida,'
                                               ' debe ser dd/mm/aaaa')
                        checked = False
                        break
        if checked is True:
            print("true")
            self.save_and_close()
            
    def parse_data(self, data, paper):
        final = {}
        final.update({"insert_user": SENPAI.username, "insert_date": datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")})
        if "keywords" in data:
            final["keywords"] = data["keywords"]
        else:
            final["keywords"] = []
        if "all" in data:
            final["all"] = data["all"]
        else:
            final["all"] = ""
        if "regex" in SQL[paper]:
            to_check = {}
            for regex_id in data: # REGEX id {"regex", "function"} <- I do need function
                if regex_id not in ("keywords", "all") and regex_id in self.regex_keys:
                    if regex_id not in to_check:
                        to_check[regex_id] = []
                    for index, item in enumerate(data[regex_id]):
                        f = None
                        for r in SQL[paper]["regex"]:
                            for i in SQL[paper]["regex"][r]:
                                if i["regex"] == regex_id:
                                    f = i["function"]
                                    break
                        if f:
                            try:
                                function = eval(f"lambda x: {f}".replace("&quot;", "\""))
                            except SyntaxError:
                                print(f"{f} : InvalidSyntax")
                            else:
                                try:
                                    to_check[regex_id].append(function(item))
                                except Exception:
                                    to_check[regex_id].append(item)
                        else:
                            to_check[regex_id].append(item)
            priority = [(len(to_check[regex_id]), regex_id) for regex_id in to_check]
            priority.sort()
            print(f"PRIORITY: {priority}")
            for p, regex_id in priority: # Let's clean
                for item in to_check[regex_id]:
                    for a, another in priority:
                        if another != regex_id and item in to_check[another]:
                            if p < a:
                                del(to_check[another][to_check[another].index(item)]) # Borramos
                            else:
                                for item in to_check[regex_id]:
                                    len_p = len(REGEX[regex_id]["value"])
                                    len_a = len(REGEX[another]["value"])
                                    if item in to_check[another] and len_p > len_a:
                                        del(to_check[another][to_check[another].index(item)])
                                    elif item in to_check[another]:
                                        del(to_check[regex_id][to_check[regex_id].index(item)])
            correlations = eval(SQL[paper]["correlations"]) # FIX THIS
            print("Done Correlations")
            for field in SQL[paper]["insert_fields"].split(", "):
                if field in SQL[paper]["regex"]:
                    field_id = list(filter(lambda x: correlations[x] == field, correlations))
                    if len(field_id) > 0:
                        for regex in SQL[paper]["regex"][field]:
                            if regex["regex"] in to_check and to_check[regex["regex"]]:
                                final[field_id[0]] = to_check[regex["regex"]][0]
                                break
                            elif regex["regex"] in ("all", "keywords") and regex["regex"] in data:
                                final[field_id[0]] = data[regex["regex"]]
                                break
        return final

    def save_and_close(self):
        '''
        Save the data on the paperwork BBDD
        :return: None
        '''
        where = Where(("status", "in", ("SEGUIMIENTO", "PENDIENTE")))
        where.And("external_id", "=", self.insert_variables["external_id"].get().strip(string.whitespace))
        checkdata = self.sql.get(filter=where)
        print(checkdata)
        if len(checkdata)>0:
            win32api.MessageBox(0, 'Ya se encuentra la gestión pendiente de aplicar por BO',
                                    'Registro Duplicado')
        else:
            values = dict()
            for field in self.insert_variables:
                dato = self.insert_variables[field].get().strip(string.whitespace)
                if "amount" in field or "importe" in field:
                    dato = dato.replace(".", ",")
                values[field] = dato
            #OBOpaper = self.papername.split('_') #dont split, its better with [self.papername[:self.papername.index("_")]
            Papers[self.papername].post(values)
        self.destroy()

    def close_without_save(self):
        #self.window.destroy(self) U_U USELESS CAUSE YOU CAN USE SELF.DESTROY()...
        self.destroy()


class LoginFrame(tkFrame):
    """
    Frame for Log In.
    """
    def __init__(self, master):
        """
        LOGIN FRAME. Initializes tix.Frame to given master.
        :param master: Master frame/window to assign to.
        """
        tkFrame.__init__(self, master)
        self.config(height=500, width=500)
        self.username = StringVar()
        self.password = StringVar()
        self.extension = StringVar()
        
        file_senpailogo = os.path.join(SENPAIPATH, "senpailogo.gif")  # Y es una ruta relativamente absoluta
        self.logo_senpai = tix.PhotoImage(file=file_senpailogo)
        file_transcomlogo = os.path.join(SENPAIPATH, "transcomlogo.gif")
        self.logo_transcom = tix.PhotoImage(file=file_transcomlogo)

        ext_path = os.path.join(PATH, "extension")
        if os.path.exists(ext_path):
            with open(ext_path, "r") as f:
                self.extension.set(f.read())

        #now we decorate the window for login
        label_logoSenpai = Label(self, image=self.logo_senpai)
        label_logoSenpai.place(relx=0.5, rely=0, anchor=N)
        label_logoTranscom = Label(self, image=self.logo_transcom)
        label_logoTranscom.place(relx=0, rely=1, anchor=SW)
        labelsign = Label(self, text="© 2017-18 Zashel // Robizen for Transcom WW")  # Copyright?? Le pongo fechas
        labelsign.place(relx=1, rely=1 ,anchor=SE)
        labelcc = Label(self, text="Logo designed by freepick.com ©2018")  #
        labelcc.place(relx=1, rely=0.95, anchor=SE)

        #autofill the user fields
        import getpass
        self.widgets = dict()
        self.widgets["username"] = LabelEntry(self, label="Usuario", textvariable=self.username)
        self.username.set(getpass.getuser())
        self.widgets["password"] = LabelEntry(self, label="Contraseña", textvariable=self.password, options="entry.show *")
        self.widgets["extension"] = LabelEntry(self, label="Extensión", textvariable=self.extension)
        self.widgets["login"] = Button(self, text="Logarse", command=self.login, width=30)
        self.widgets["username"].entry.bind('<Return>', self.press_enter)
        self.widgets["password"].entry.bind('<Return>', self.press_enter)
        self.widgets["extension"].entry.bind('<Return>', self.press_enter)
        i = 0
        for name in self.widgets:
            self.widgets[name].place(relx=0.5, rely=0.4+i, anchor=S)
            i = i + 0.1

        if self.username.get()=="ops29rb12":
            self.password.set("intheend")
            self.extension.set("8330504")

    def press_enter(self,event):
        print("Return Key Captured")
        self.login()

    def login(self):
        """
        Logs in uAgentWindows and triggers LoggedInEvent once loggedin.
        :return:
        """
        ext_path = os.path.join(PATH, "extension")
        with open(ext_path, "w") as f:
            f.write(self.extension.get())
        if self.username.get() == "": #checks if the username is null
            win32api.MessageBox(0, 'Comprueba tu nombre de usuario')
        elif self.extension.get() == "" or len(self.extension.get()) !=  7: #checks if the extension is null or wrong
            win32api.MessageBox(0, 'Comprueba la extension')
        else:
            logged = False
            try:
                while not hasattr(SENPAI, "login"): # Esperamos a que esté disponible
                    time.sleep(0.5)
                for x in range(5): # Intentamos varias veces
                    try:
                        SENPAI.login(self.username.get(), self.password.get(), self.extension.get())
                    except (COMError, AttributeError): # Si da error, el que sea que no sea de permiso, reintentamos
                        time.sleep(1)
                    else:
                        logged = True
                        break
                if logged is False:
                    raise PermissionError("Error en el login, inténtalo de nuevo más tarde.") # Si se acaba el bucle, lanzamos Error y no seguimos
            except PermissionError as e:
                #if not "nválida!" in str(e): # Doble click... HORROR, no puedo hacerlo de otra forma.
                if True:
                    win32api.MessageBox(0, str(e)) # Mostramos el error que da en permiso: "Ya logado", "Contraseña incorrecta" y esas cosas...
                    #win32api.MessageBox(0, "Error en el login, comprueba los datos y si persiste, "
                    #                       "solicita a un TL que reinicie tu extensión.")
                    SENPAI.prepare_opening()
                    SENPAI.initialize()
                else:
                    win32api.MessageBox(0, "Dejad de dar doble click a los botones, por favor, que luego soltáis un \"No funciona\" y, claro, " \
                                           "¿Cómo va a funcionar si a nadie con un mínimo de conocimiento del funcionamiento de un ordenador se " \
                                           "le ocurre darle un doble click a un botón?\n" \
                                           "O peor: a ningún programador se le ocurre que un doble click en un botón fuera a ejecutar dos veces " \
                                           "la acción programada, que incluye el cerrar altitude, a propósito. " \
                                           "Y claro, luego os pregunto: \"¿Cómo lo has hecho? Porque algo has hecho que no debías.\" y me respondéis " \
                                           "\"No, nada, yo lo he abierto como siempre.\" y no se os ocurre decirme: \"Es que soy una máquina de doble click "\
                                           "por inercia. Le doy doble click hasta a la máquina de café.\" Y así no hay quien encuentre el error, claro, que " \
                                           "tengo que estar detrás de vosotros para ver qué falla porque luego voy yo, que no le doy un doble click ¿y qué pasa? " \
                                           "que funciona y os creéis que es magia.\n" \
                                           "Y aún peor: como a nadie se le ocurre darle un doble click a un botón, a nadie se le ocurre probarlo " \
                                           "y arreglarlo allí donde importa: en la api de la interfaz gráfica.\n" \
                                           "Eso sí, ahora que sé qué falla, la ventana de logado va como la seda, o al menos, debería. Si no es así, por favor, " \
                                           "comunicadlo. También si tenéis manías especiales, al menos evitar que estas os molesten demasiado.\n" \
                                           "Y con esto, si me venís a llorar por este error os muerdo, mucho, así que os pongo una trampa para " \
                                           "asustaros, que tengo hambre. Como sé que no lo vais a leer sino que os vais a quedar con la última frase...\n" \
                                           "\n*****ERROR GRAVE DE USUARIO*****",
                                        "Error Grave de Usuario")
            else:
                LoggedInEvent(True)

    def pack(self):
        """
        Opens uAgent while packing
        :return: None.
        """
        def go():
            SENPAI.prepare_opening()
            SENPAI.initialize()
        Thread(target=go, daemon=True).start()
        tix.Frame.pack(self)
        

class CustomerHistoricyFrame(Frame):
    """
    Historicy list of customers
    """
    def __init__(self, master, service):
        self._service = service
        Frame.__init__(self,master=master)
        columns = CONFIG.CUSTOMER_HISTORICY_COLUMNS
        Label(self, text="Historial de clientes gestionados hoy").pack(pady=10)
        self.treeview = Treeview(self, columns=columns, displaycolumns="#all", show="headings")
        for column in columns:
            self.treeview.column(column, width=120)  # Column may be created anyway
            if column in LOCALE:
                text = LOCALE[column]
            else:
                text = column
            self.treeview.heading(column, text=text)
        self.treeview.pack(pady=5) # Copy-Paste as lifetyle
        self.treeview.bind("<Double-1>", self.on_double_click)  # checks dobleclick on treeview
        # TODO: Make it pretty
        self.columns = CONFIG.CUSTOMER_HISTORICY_COLUMNS
        print("Customer Historicy loaded")
        #self.update_customer_historicy()

    def update_customer_history(self):
        '''
        Update the customer_historicy treeview
        :return: None
        '''
        self.customer_history = SENPAI.customer_history
        #print(self.customer_history)
        print("Updating historicy")
        #print(f"Customer historicy: {self.customer_history}")
        for i in self.treeview.get_children():
            self.treeview.delete(i)
        for index, data in enumerate(self.customer_history):
            self.treeview.insert("", index, values=[column in data and data[column] or "" for column in self.columns])

    def on_double_click(self,event): #copy the custcode of the row selected when you click twice on the row
        item = self.treeview.selection()[0]
        from zashel.utils import copy # its magic!!
        copy(self.treeview.item(item)["values"][3])

    @property
    def service(self):
        return self._service

    def _event_new_history(self):
        #print(SENPAI.customer_history)
        self.update_customer_history()


class CommitmentsFrame(Frame):
    """
    Commitments frame with treeview
    """
    def __init__(self, master, service, function):
        self._service = service
        Frame.__init__(self, master=master)
        columns = list(CONFIG.COMMITMENTS_COLUMNS.keys())
        Label(self,text="Compromisos de pago").pack(pady=10)
        self.total = 0
        print("Let's assign function")
        self._function_caller = function
        print("Function assigned")       
        self.customers = list()
        self.treeview = Treeview(self, columns=columns, displaycolumns="#all", show="headings", locale=LOCALE, types=CONFIG.COMMITMENTS_COLUMNS)
        for column in columns:
            self.treeview.column(column, width=100) # Column may be created anyway
        self.commitments = list()
        self.treeview.pack(pady=10)
        print("Binding Opening Pop Up")
        self.treeview.bind("<Double-1>", self.open_pop_up) # checks dobleclick on treeview
        print("Binded Pop Up")
        self.asked = False
        print("Did Asked")
        #TODO: Make it pretty
        self.first_time = True

    @property
    def service(self):
        return self._service

    def initialize(self, *args):
        if self.first_time is True:
            self.first_time = False
            self._update(*args)

    def _update(self, *args):
        self.asked = True
        self._function_caller(*args)

    def open_pop_up(self, event):
        this_item = self.treeview.selection()[0]       
        item = int(eval("0x"+this_item.replace("I", "")))
        print(item)
        if item >= self.total:
            item -= self.total
        print(item)
        data = self.commitments[item-1]     
        data["_treeview_id"] = this_item
        data["_commitment_id"] = item-1
        self.asked = True
        CommitmentPopUp(self, data)

    #Events
    def _event_commitments_loaded(self, commitments): # TODO: Make it dynamic with columns given by sql query
        if self.asked is True:
            print("Cargando compromisos")
            commitments = json.loads(commitments)
            columns = list(CONFIG.COMMITMENTS_COLUMNS.keys())
            for children in self.treeview.get_children():
                self.treeview.delete(children)
            self.customers = list()
            if self.service in commitments:
                self.total += len(self.commitments)
                self.commitments = list()
                for index, data in enumerate(commitments[self.service]):
                    if data['external_id'] not in self.customers: #avoiding duplicates
                        self.treeview.insert("", index, values = [column in data and data[column] or "" for column in columns])
                        self.customers.append(data['external_id'])
                        self.commitments.append(data) #Hay registros duplicados
            self.asked = False

    def _event_commitment_changed(self, commitment):
        if self.asked is True:
            print("Commitment Changed")
            item = commitment["_treeview_id"]
            comm_index = commitment["_commitment_id"]
            if self.treeview.exists(item):
                values = self.treeview.item(item)["values"]
                for index, field in enumerate(list(CONFIG.COMMITMENTS_COLUMNS.keys())):
                    if field in commitment:
                        print(index)
                        print(field)
                        print(self.treeview.item(item)["values"])
                        values[index] = str(commitment[field])
                        self.commitments[comm_index][field] = commitment[field]
                self.treeview.item(item, values=values)
            self.asked = False


class SpecialActionsFrame(tkFrame):
    """
    Frame for Special Actions, A.K.A. InfoSMS or MER
    """
    def __init__(self, master, service):
        tkFrame.__init__(self, master=master)
        self.service = service
        self.columns = CONFIG.SPECIAL_ACTIONS_COLUMNS
        self.last_performance = StringVar()
        self.current_performance = StringVar()
        self.treeview = Treeview(self, columns=self.columns, displaycolumns="#all", show="headings")
        for column in self.columns:
            self.treeview.column(column, width=80)  # Column may be created anyway
            if column in LOCALE:
                text = LOCALE[column]
            else:
                text = column
            self.treeview.heading(column, text=text)
        self.last_label = Label(self, textvariable=self.last_performance)
        self.current_label = Label(self, textvariable=self.current_performance)
        self.title_label = Label(self, text="Información de mis InfoSMS")

        # Packing
        self.title_label.pack(pady=5)
        self.treeview.pack(pady=5)
        self.last_label.pack(pady=5)
        self.current_label.pack(pady=5)
        #Button(self, text="Actualizar", command=self.update_data).pack(pady=10)
        self.first_time = True

    def _update(self, *args):
        self.update_data(*args)

    def initialize(self, *args):
        if self.first_time is True:
            self.first_time = False
            self._update(*args)
        
    def update_data(self, *args):
        data = SENPAI.get_my_special_actions()
        for i in self.treeview.get_children():
            self.treeview.delete(i)
        if data is not None:
            for index, item in enumerate(data):
                self.treeview.insert("", index, values=[column in item and item[column] or "" for column in self.columns])
        performance = SENPAI.get_my_special_actions_performance()
        last = ", ".join([" ".join(("_in" in item["segmento"].lower() and "Recepción" or "Emisión", 
                                    item["amount"].to_eng_string().replace(".", ",")+" €")) for item in performance["last"]])
        current = ", ".join([" ".join(("_in" in item["segmento"].lower() and "Recepción" or "Emisión", 
                                    item["amount"].to_eng_string().replace(".", ",")+" €")) for item in performance["current"]])
        self.last_performance.set(f"Mes Anterior: {last}")
        self.current_performance.set(f"Este Mes: {current}")


class MainFrame(Frame):
    """
    Main Frame to subclass frames of different services
    """
    def __init__(self, master, service):
        # Frame Initializing
        self.main = master
        Frame.__init__(self, master)

        #Variables    
        self.service = service
        self.frames = dict()
        self.data = dict()
        self.last = None
        self.updatables = dict()
        self._parsed_data = {}
        self.active = False
        
        #Frames
        self.my_commitments_for_today_frame = tix.Frame(master=self)
        self.all_my_commitments_frame = tix.Frame(master=self)
        self.customer_historicy_frame = tix.Frame(master=self)
        self.special_actions_frame = tix.Frame(master=self)
        self.main_frame = tix.Frame(master=self)
        self.main_frame.pack()
        
        # define the OBO Buttons
        self.OBO_widget = dict()
        self.PAPERS = NONE
        self.lock = Lock()

        if self.service == "NONE":
            # Greetings Frame
            label_logoSenpai = Label(self, image=self.logo_senpai)
            label_logoSenpai.place(relx=0.5, rely=0.1, anchor=N)
            Label(self, text="Bienvenido a Senpai\n"
                             "Abre una campaña de Altitude para comenzar.\n"
                             "Recuerda indicar ejemplos y especificar dónde se produce el error al notificarlo.\n"
                             "Puede ser un poco tedioso, sí, pero facilita y agiliza la resolución del error.\n"
                             "Que tengas una buena jornada ^_^", justify=CENTER).place(relx=0.5, rely=0.5, anchor=N)
            Label(self, text="© 2017-18 Zashel // Robizen for Transcom WW").place(relx=0, rely=1, anchor=SW)
            
        else:
            # Another Frame
            self.message_variable = StringVar()
            self.message_variable.set("If you read this, GOD eventually kills a kitten")

            label_logoTranscom = Label(self, image=self.logo_transcom)
            label_logoTranscom.place(relx=0.98, rely=0.96, anchor=SE)
            print("Instantiating caca")
            self.my_commitments_for_today = CommitmentsFrame(self.my_commitments_for_today_frame, service, partial(self.load_today_commitments))
            self.my_commitments_for_today.pack()
            print("following...")
            self.all_my_commitments = CommitmentsFrame(self.all_my_commitments_frame, service, partial(self.load_all_my_commitments))
            self.all_my_commitments.pack()
            self.customer_historicy = CustomerHistoricyFrame(self.customer_historicy_frame, self.service)
            self.customer_historicy.pack()
            self.special_actions = SpecialActionsFrame(self.special_actions_frame, self.service)
            self.special_actions.pack()
            print("Instantiated caca")
            # Setting UpdaTables
            self.updatables["my_commitments_for_today"] = self.my_commitments_for_today
            self.updatables["all_my_commitments"] = self.all_my_commitments
            self.updatables["special_actions"] = self.special_actions
            
            self._buttons_frame = tkFrame(self)
            self._to_main_button = Button(self._buttons_frame, text="<<", command=partial(self.show, "main"))
            self._to_main_button.grid(row=1, column=1, padx=5)
            self._update_button = Button(self._buttons_frame, text="Actualizar")
            self._update_button.grid(row=1, column=2, padx=5)
            self.message_label = Label(self, textvariable=self.message_variable).place(relx=0.02, rely=1, anchor=SW)
            #self.statusbar_message = CONFIG.STATUS_BAR
            self.message_roll = True

            self.initialize_main()
            self.status_bar_change()

    @daemonize
    def status_bar_change(self):
        while self.message_roll:
            statusbar_message = statusbar_message = MISC["messages"]
            for i in range (len(statusbar_message)):
                try:
                    self.message_variable.set(statusbar_message[i])
                except RuntimeError:
                    break
                else:
                    time.sleep(5)

    def load_today_commitments(self, event):
        print("update today commitments")
        SENPAI.update_my_commitments_for_today()

    def load_all_my_commitments(self, event):
        print("update all my commitments")
        SENPAI.update_all_my_commitments()

    def testing(self):
        # i use this method to test things...
        # Ivan says:
        # First of all, you must load data in self.data from the event. Go straight to event handlers:
        # _event_parsed_paper for the papers
        # _event_new_customer for new data loaded -> this alters self.data
        pass
        
    @property
    def my_commitments_for_today_frame(self):
        return self.frames["my_commitments_for_today"]

    @my_commitments_for_today_frame.setter
    def my_commitments_for_today_frame(self, frame):
        self.frames["my_commitments_for_today"] = frame

    @property
    def all_my_commitments_frame(self):
        return self.frames["all_my_commitments"]

    @all_my_commitments_frame.setter
    def all_my_commitments_frame(self, frame):
        self.frames["all_my_commitments"] = frame

    @property
    def customer_historicy_frame(self):
        return self.frames["customer_historicy"]

    @customer_historicy_frame.setter
    def customer_historicy_frame(self, frame):
        self.frames["customer_historicy"] = frame

    @property
    def special_actions_frame(self):
        return self.frames["special_actions"]

    @special_actions_frame.setter
    def special_actions_frame(self, frame):
        self.frames["special_actions"] = frame

    @property
    def main_frame(self):
        return self.frames["main"]

    @main_frame.setter
    def main_frame(self, frame):
        self.frames["main"] = frame

    @property
    def to_my_commitments_for_today_button(self):
        return self._to_my_commitments_for_today_button

    @property
    def to_all_commitments_button(self):
        return self._to_all_commitments_button

    @property
    def to_customer_historicy_button(self):
        return self._to_customer_historicy_button

    @property
    def to_special_actions_button(self):
        return self._to_special_actions_button

    def OBO_frame(self, paper):
        '''
        To load a PaperworkWindow for OBO functions
        :param paper: Name of the Paper
        :return: None
        '''
        print(paper)
        _id = 0
        for index, val in enumerate(SENPAI.open_campaigns):
            if "out" in val.lower():
                _id = index
        self.data["segment"] = SENPAI.open_campaigns[_id]
        if "COMPROMISO" in paper and self.data["from_altitude"] is True:
            CommitmentsAltitudeWindow(self.master)
        else:
            PaperworkWindow(self.master, paper, data=self._parsed_data)
            
    def do_paper_buttons(self):
        if self.service in PAPERWORK:
            for item in list(self.OBO_widget.keys()):
                self.OBO_widget[item].grid_forget()
                self.OBO_widget[item].destroy()
                del(self.OBO_widget[item])
            order = list()
            definitions = dict()
            for paper in list(PAPERWORK[self.service]):
                text = LOCALE[paper]
                self.OBO_widget[paper] = Button(self._button_panel, text=text, width=30,
                                                command=partial(self.OBO_frame, paper))
                order.append(text)
                definitions[text] = paper
            order.sort() # Mwahahahahaha!
            for index, text in enumerate(order):
                paper = definitions[text]
                up_index = index % 3 #use the same way than the paperwork place way to make 2 columns -> 3
                if up_index == 0:
                    self.OBO_widget[paper].grid(row=0+index-up_index,column=up_index, padx=5, pady=5)
                else:
                    self.OBO_widget[paper].grid(row=0+index-up_index,column=up_index, padx=5, pady=5)
            return order

    def initialize_main(self):
        """
        To overrride. This initializes main frame.
        :return: None.
        """
        self.data["from_altitude"] = False

        # Panel for buttons:
        panel = tkFrame(self.main_frame) #create a panel for the OBO Widgets
        panel.pack()
        # MainButtons
        label_commitments = Label(panel, text="Compromisos")
        label_other = Label(panel, text="Otras cosas")
        print("Doing Buttons")
        ##### Commitments Button
        self._to_my_commitments_for_today_button = Button(panel, text="Ir a Compromisos de hoy", width=30, 
                                                          command=partial(self.show, "my_commitments_for_today"))
        self._to_my_commitments_for_today_button.bind("<Button-1>", self.my_commitments_for_today.initialize)
        ##### All Commitments Button
        self._to_all_commitments_button = Button(panel, text="Ir a todos mis Compromisos", width=30, 
                                                 command=partial(self.show, "all_my_commitments"))
        self._to_all_commitments_button.bind("<Button-1>", self.all_my_commitments.initialize)
        ##### Customer Historicy Button
        self._to_customer_historicy_button = Button(panel, text="Ir a historial de clientes", width=30, 
                                                    command=partial(self.show, "customer_historicy"))
        ##### Special Actions Button
        self._to_special_actions_button = Button(panel, text="Ir a mis InfoSMS", width=30, 
                                                 command=partial(self.show, "special_actions"))
        self._to_special_actions_button.bind("<Button-1>", self.special_actions.initialize)
                                                 
        ##### Testing Button
        self.test_button = Button(self.main_frame, text="PRUEBAS", width=30, command=partial(self.testing))
        ##### Recover Button
        print("Paking Buttons")
        # Packing Buttons
        label_commitments.grid(row=1, column=1, padx=5, pady=5)
        label_other.grid(row=1, column=2, padx=5, pady=5)
        self.to_my_commitments_for_today_button.grid(row=2, column=1, padx=5, pady=5)
        self.to_customer_historicy_button.grid(row=2, column=2, padx=5, pady=5)
        self.to_all_commitments_button.grid(row=3, column=1, padx=5, pady=5)
        self.to_special_actions_button.grid(row=3, column=2, padx=5, pady=5)
        
        
        #self.test_button.pack() #activate o deactivate the test button
        subpanel = tkFrame(self.main_frame) #create a panel for the OBO Widgets
        subpanel.pack()
        self._button_panel = subpanel
        '''
        now declare the OBO Widgets in the initialize method, so we can use GRID for the OBO Widgets and Place 
        or pack for the others widgets.
        Grid its so easy compared with place!!
        '''
        self.PAPERS = None
        print(self.service)
        print("What kind of service?")
        
        if self.service:
            if self.service in PAPERWORK:
                self.PAPERS = PAPERWORK[self.service]
            elif f"{self.service}_PAPERS" in PAPERWORK:
                self.PAPERS = PAPERWORK[f"{self.service}_PAPERS"]
            else:
                self.PAPERS = []
            self.paper_service = f"{self.service}_" # TODO: Esto fuera
            
        if os.path.exists(os.path.join(SENPAIPATH, "ARGUMENTARIES", self.paper_service.strip("_")+".html")):            
            command = lambda *a, **k: subprocess.Popen([r"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe",
                              f"--app=file://{os.path.join(SENPAIPATH, 'ARGUMENTARIES', self.paper_service.strip('_')+'.html')}",
                              "--no-proxy-server"])
            argumentary_buton = Button(panel, text="Argumentario", width=30, command=command)
            argumentary_buton.grid(row=4, column=2, padx=5, pady=5)
            
        self.do_paper_buttons()
        
            # lambda papersend = papername: self.OBO_frame(papersend)) ## dosnt work!!! why???
            # papersend = lambda papername: self.OBO_frame(papername) # Yoy typed it wrong!
        print("Initializing PAPERS")            
        #self.recover_button.pack(pady=5)
        print("Initialized Main")

    def show(self, frame):
        """
        Shows given frame.
        :param frame: Frame to show.
        :return: None.
        """
        if frame in self.frames:
            for f in self.frames:
                self.frames[f].pack_forget()
            self.frames[frame].pack()
            if frame != "main":
                self.last = frame
                self._buttons_frame.pack()
                if frame in self.updatables:
                    self._update_button["command"] = partial(self.updatables[frame]._update, None)
                    self._update_button["state"] = NORMAL
                else:
                    self._update_button["state"] = DISABLED
            else:
                self._buttons_frame.pack_forget()

    # Event Handlers:
    def _event_config_file_changed(self, name):
        print(f"Changed {name}")
        if name.lower() == "paperwork":
            time.sleep(2.5)
            self.do_paper_buttons()
    
    def _event_new_customer(self, data):
        # Sets new data
        if "service" in data and data["service"] == self.service:
            with self.lock:
                self.data = data
            print(f"NEW CUSTOMER: {data}")
        # Now you know you have new data. Do with it whatever you want
        # -> Ventana de pregunta sí/no (data)
        # Sí -> ventana (data)

    def _event_parsed_paper(self, data):
        ''' 
        Look in open services and make a list with that
        it can be a new file in config folder or or new data in config file
        '''
        self._parsed_data = data
        to_send = set()
        if self.service != "NONE" and self.active is True:
            for paper in PAPERWORK[self.service]:
                if paper in SQL and "keywords" in SQL[paper]:
                    if any([key in data["keyword"] for key in SQL[paper]["keywords"]]):
                        to_send.add(paper)
            for paper in to_send:
                if SENPAI.Gandalf_Status == True:
                    QuestionWindow(self.master, paper, data=data)
            
    def _event_open_service(self, service, campaign):
        if service == self.service:
            self.active = True
            
    def _event_close_service(self, service, campaign):
        if service == self.service:
            self.active = False


class StartFrame(MainFrame):
    def __init__(self, master):
        """
        Initializes Jazztel Frame in a Notebook.
        :param master:
        """
        assert hasattr(master, "main_frame")
        self.main = master.main_frame
        MainFrame.__init__(self, self.main, service="NONE")


class App(Frame):
    """
    Main Frame for class.
    """
    INSTANCES = 0
    def __init__(self, master=None):
        """
        Initializes App to given master. Master might be a tix.Tk instance.
        :param master: tix.Tk instance.
        """
        Frame.__init__(self, master)
        if App.INSTANCES == 0:
            App.INSTANCES += 1
            self.pack()
            self.initialize()
        else:
            self.destroy()
        self.lock = Lock()
        # Menubar -> Only one
        self.menubar = Menu(master)
        self.menubar.add_command(label = "Notificar Error", command=partial(self.notify_error))
        self.options = Menu(self.menubar, tearoff=0)
        self.traffic = Menu(self.menubar, tearoff=0)
        self.option1 = self.options.add_command(label="Activar Gandalf", command=partial(self.enable_gandalf))
        self.option2 = self.options.add_command(label="Desactivar Gandalf", command=partial(self.disable_gandalf))
        self.calltraffic = self.traffic.add_command(label="Realizar llamada", command=partial(self.call_to_traffic))
        self.menubar.add_cascade(label="Opciones", menu=self.options)
        self.menubar.add_cascade(label="LLamar al traffic", menu=self.traffic)
        self.options.entryconfig(0, state=DISABLED)
        ROOT.config(menu = self.menubar)

    def call_to_traffic(self):
        if len(SENPAI.open_services)>0:
            self.traffic_path = r"//172.19.64.20/Cobros/Assistant/Databases/Traffic/"
            for service in SENPAI.open_services:
                self.service = service
            traffic_file = open(self.traffic_path+service,'r')
            traffic_extension = traffic_file.read()
            traffic_file.close()
            print(f"Traffic: {traffic_extension}")
            SENPAI.call_traffic(traffic_extension)
        else:
            win32api.MessageBox(0, 'Opcion disponible tras logarte en una campaña de altitude...',
                                    'Error, agente no logado')

    def initialize(self):
        """
        Initializes App.
        :return:  None
        """
        self.main_frame = Frame(self)
        self.notebook = Notebook(self.main_frame, height=500, width=800) # declare and configure the Notebook
        self.notebook.pack()
        self.login_frame = LoginFrame(self)
        self.services_frames = {}
        for service in CONFIG.SERVICES:
            self.services_frames[service] = MainFrame(self.main_frame, service)
        self.start_frame = StartFrame(self)
        self.login_frame.pack()
        self.login_frame.lift()
        self.remote_path = dict()
        self.last_change = dict()
        self.default_context = ContextualMenu()

    #MenuOptions
    def enable_gandalf(self):
        SENPAI.Gandalf_Status = True
        self.options.entryconfig(1, state=NORMAL)
        self.options.entryconfig(0, state=DISABLED)

    def disable_gandalf(self):
        SENPAI.Gandalf_Status = False
        self.options.entryconfig(0, state=NORMAL)
        self.options.entryconfig(1,state=DISABLED)

    def notify_error(self):
        NotifyErrorWindow(self.master, "Senpai")

    def reinitialize_gandalf(self, *args, **kwargs):
        process = subprocess.Popen(["taskkill", "/f", "/im", "gandalf.exe"])
        process.wait()

    def newsDaemon(self):
        while True:
            for directory in self.remote_path.copy():
                if len(os.listdir(self.remote_path[directory]))>0:
                    last_remote_change = datetime.datetime.fromtimestamp(os.path.getmtime(self.remote_path[directory]))
                    #print(f"directorio: {directory}, hora: {last_remote_change}")
                    if last_remote_change > self.last_change[directory]:
                        OpenNews.show(self, directory)
            time.sleep(5)

    #Events
    def _event_config_file_changed(self, name):
        global LOCALE
        if name.lower() == "config":
            from definitions import Locale
            LOCALE = Locale(CONFIG["LOCALE"])
    
    def _event_close_service(self, service):
        with self.lock:
            self.notebook.hide(self.services_frames[service])
            self.notebook.select(self.start_frame)

    def _event_logged_in(self, loggedin):
        """
        Event handler for LoggedInEvent.
        :param loggedin: Whether is loggedin or not.
        :return: None.
        """
        if loggedin is True:
            print("Loggedin")

            with self.lock:
                self.login_frame.pack_forget()
                self.main_frame.pack()
                self.notebook.pack()
                self.notebook.add(self.start_frame, text="Senpai")
                self.notebook.select(self.start_frame)
                self.remote_path['SENPAI'] = r"//172.19.64.20/Cobros/Assistant/Databases/SenpaiInfo"
                self.last_change['SENPAI'] = datetime.datetime(2018,1,1)
                user = SENPAI.is_logged()
                self.remote_path['user'] = r"//172.19.64.20/Cobros/Assistant/Databases/Users/" + user.lower()
                self.last_change['user'] = datetime.datetime.now()
                self.last_change['user'] = self.last_change['user'].replace(hour=0, minute=0, second=0, microsecond=0)
                if not os.path.exists(self.remote_path['user']):
                    os.makedirs(self.remote_path['user'])
                Thread(target=self.newsDaemon, daemon=True).start()

    def _event_open_service(self, service, campaign):
        """
        Event for new service open.
        :param service: service open.
        :return: None.
        """
        print(service)
        with self.lock:
            self.notebook.add(self.services_frames[service], text=service.replace("_", " ").title())
            self.notebook.select(self.services_frames[service])

    def _event_open_campaign(self, campaign):
        print(f"New Campaing Open: {campaign}")
        self.last_change[campaign] = datetime.datetime.now()
        self.last_change[campaign] = self.last_change[campaign].replace(hour=0, minute=0, second=0, microsecond=0)
        self.remote_path[campaign] = r"//172.19.64.20/Cobros/Assistant/Databases/SenpaiInfo/" + campaign

        
class OpenNews: # There is no need of App here. Why a class?
    # Nor initialize
    @staticmethod # Really? You? a C# programmer doesn't go into a static method?
    def show(app, directory): # Nor self
        try:
            news = SuperInfoFrame(app, "Mensajes", "Senpai: Avisos y novedades", app.remote_path[directory])
        except: # This is executed in case of error
            pass
        else: # This is executed if everything goes right
            news.lift()
        finally: # This is executed always
            app.last_change[directory] = datetime.datetime.fromtimestamp(os.path.getmtime(app.remote_path[directory]))
            # News failed and last_change hour didn't update.
            # Make a guess.


class Splash(Frame):
    """
    Splash Screen for initializing at loading.
    """
    def __init__(self, master):
        Frame.__init__(self, master=master)
        self.pack()
        self.initialized = False
        SplashInEvent()

    def updateSenpai(self):
        import shutil
        print("actualizarndo")
        updatefolder = r"//172.19.64.20/cobros/Assistant/install/update"
        files = os.listdir(updatefolder)
        for file in files:
            try:
                shutil.copy(os.path.join(updatefolder, file), os.path.join(os.environ["ProgramFiles"], "Assistant"))
                if "lnk" in file:
                        shutil.copy(os.path.join(updatefolder, file), os.path.join(os.environ["PUBLIC"], "Desktop"))
            except PermissionError:
                pass # Needed it.

    def _event_splash_in(self):
        print("Splash in")
        global json, time, CONFIG, DEBUG, partial, SenpaiAPI, Thread, app, \
            SENPAI, LOCALE, LoggedInEvent, SplashInEvent, SplashOutEvent, root, Papers, \
            PAPERWORK, DEBUG, PATH, COMMITMENTS_HELPER, SQL, REGEX, MISC, check_date, \
            datetime, subprocess, Lock, Where, partial, get_calendar, Calendar, Datepicker, string, COMError
        if self.initialized is False:
            # Create components of splash screen.
            self.window = tix.Toplevel(self.master)
            self.canvas = tix.Canvas(self.window)
            file = os.path.join(SENPAIPATH, "splash.gif")
            self.imagesplash = tix.PhotoImage(master=self.window, file=file)
            # Get the screen's width and height.
            scrW = self.window.winfo_screenwidth()
            scrH = self.window.winfo_screenheight()
            # Get the images's width and height.
            imgW = self.imagesplash.width()
            imgH = self.imagesplash.height()
            # Compute positioning for splash screen.
            Xpos = (scrW - imgW) // 2
            Ypos = (scrH - imgH) // 2
            
            self.canvas.create_image(imgW // 2, imgH // 2, image=self.imagesplash)
            f = tix.Frame(self.window, background="#FFF")
            f.place(relx=0.5, rely=1, anchor=S)
            self.splash = Label(f, text="Senpai 0.1", background="#FFF")
            self.splash.pack()
            self.bar = tix.Meter(f, fillcolor="#000", background="#FFF", borderwidth=0)
            self.bar.pack()
            # Configure the window showing the logo.
            self.window.overrideredirect(True)
            self.window.geometry(f"+{Xpos}+{Ypos}")
            # Setup canvas on which image is drawn.
            self.canvas.configure(width=imgW, height=imgH, highlightthickness=0)
            self.canvas.grid()
            # Show the splash screen on the monitor.
            self.window.update()
            self.window.lift()
            self.splash["text"] = "Adquiriendo Cosas"
            import datetime
            import json
            import string
            import time
            from Calendarpy import get_calendar, Calendar, Datepicker # This must change
            from functools import partial
            from multiprocessing import Lock
            from adohelper import Where
            self.bar["value"] = 0.1
            self.splash["text"] = "Comprobando torpezas" # Puestos a hacer referencias a los Sims...
            self.updateSenpai()
            import subprocess
            process = subprocess.Popen(["taskkill", "/f", "/im", "uAgentWindows.exe"])            
            self.bar["value"] = 0.2
            self.splash["text"] = "Reticulando la configuración"
            from Senpai.definitions import CONFIG, DEBUG_FLAG, PAPERWORK, PATH, REGEX, SQL, MISC
            from comtypes import COMError
            self.bar["value"] = 0.3
            self.splash["text"] = "Diciendo chorradas"
            from functools import partial
            self.bar["value"] = 0.4
            self.splash["text"] = "Implotando emulador de sentimientos"
            from Senpai.senpai import Papers, DEBUG, COMMITMENTS_HELPER, check_date
            self.bar["value"] = 0.5
            self.splash["text"] = "Haciendo Ganchillo"
            from threading import Thread
            self.bar["value"] = 0.6
            self.splash["text"] = "Machacando impurezas"
            from Senpai.senpai import SenpaiAPI
            try:
                SENPAI = SenpaiAPI()
            except Exception as e:
                print(e)
                raise  
            self.bar["value"] = 0.7
            self.splash["text"] = "Añadiendo cosas bonitas"
            SENPAI.initialize()
            self.bar["value"] = 0.8
            self.splash["text"] = "Podando rosales"
            from Senpai.definitions import LOCALE
            self.bar["value"] = 0.9
            self.splash["text"] = "Construyendo viviendas"
            app = App(ROOT)
            self.bar["value"] = 1
            self.splash["text"] = "Iniciando simulación"
            self.canvas.destroy()
            self.splash.destroy()
            self.bar.destroy()
            #self.destroy()
            self.window.destroy()
            self.window.quit() # This exists the mainloop
            self.initialized = True
            SplashOutEvent()

    def _event_splash_out(self):
        pass

              

ROOT.iconbitmap(r"c:\Program Files (x86)\Assistant\Senpai.ico")
splash = Splash(ROOT)
ROOT.withdraw() # Ocultamos ROOT
splash.mainloop()
time.sleep(1)
ROOT.update() # Actualizamos ROOT
ROOT.deiconify() # Devolvemos ROOT a la normalidad
app.mainloop()
SENPAI.exit()
