import comtypes, comtypes.client
import os
#import pythoncom
from Mordor import Council
from MordorManager import get_api
from zashel.gapi import SCOPE
from zashel.transcom import TranscomAPI


SCOPES = [SCOPE.DRIVE, SCOPE.SPREADSHEETS]

'''
with open("secret.txt") as f:
    secret = t.read()

def get_senpai(): #Deprecated
    """
    Gets the API for Google and logs in
    :return: the API
    """
    pythoncom.CoInitialize()
    _api = TranscomAPI(scopes=SCOPES,
                       secret_data={"installed": {
                           "client_id": "137432867660-g65uia96vl61olij75crum7qh0dl9c8b.apps.googleusercontent.com",
                           "project_id": "assistant-updater", "auth_uri": "https://accounts.google.com/o/oauth2/auth",
                           "token_uri": "https://accounts.google.com/o/oauth2/token",
                           "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
                           "client_secret": secret,
                           "redirect_uris": ["urn:ietf:wg:oauth:2.0:oob",
                                             "http://localhost"]}})
    _api.login()
    _api.teamdrive_open("Senpai")
    return _api

def get_historicy(): #Deprecated
    """
    Gets the API for Google and logs in
    :return: the API
    """
    pythoncom.CoInitialize()
    _api = TranscomAPI(scopes=SCOPES,
                       secret_data={"installed": {
                           "client_id": "137432867660-g65uia96vl61olij75crum7qh0dl9c8b.apps.googleusercontent.com",
                           "project_id": "assistant-updater", "auth_uri": "https://accounts.google.com/o/oauth2/auth",
                           "token_uri": "https://accounts.google.com/o/oauth2/token",
                           "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
                           "client_secret": secret,
                           "redirect_uris": ["urn:ietf:wg:oauth:2.0:oob",
                                             "http://localhost"]}})
    _api.login()
    _api.teamdrive_open("Historicy")
    return _api
'''

class Locale(dict):
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            return item
            

get_senpai = get_api
get_historicy = get_api

SENPAI = None
HISTORICY = None
PATH = os.path.join(os.environ["APPDATA"], "Senpai")
REMOTE_PATH = r"//172.19.64.20/cobros/Assistant/Config"
if not os.path.exists(PATH):
    os.makedirs(PATH)
CONFIG = Council(None, name="Config", path=PATH, remote_path=REMOTE_PATH)
PAPERWORK = Council(None, name="Paperwork", path=PATH, remote_path=REMOTE_PATH)
SQL = Council(None, name="Sql", path=PATH, remote_path=REMOTE_PATH)
REGEX = Council(None, name="Regular Expressions", path=PATH, remote_path=REMOTE_PATH)
MISC = Council(None, name="Misc", path=PATH, remote_path=REMOTE_PATH)
LOCALPATH = os.path.join(os.environ["LOCALAPPDATA"], "senpai")
DEBUG_FLAG = True
LOCALE = Locale(CONFIG.LOCALE)


#ORANGE_PAPERS = PAPERWORK.ORANGE_PAPERS
#JAZZTEL_PAPERS = PAPERWORK.JAZZTEL_PAPERS


PROGRAMDATA = os.path.join(os.environ["PROGRAMDATA"], "Senpai")
if not os.path.exists(PROGRAMDATA):
    os.makedirs(PROGRAMDATA)
    
    
def create_db(path, password=None): # This to create a database and use it instead of originals.
    #assert os.path.exists(path)
    print(path)
    catalog = comtypes.client.CreateObject("ADOX.Catalog") # This serves to create a new DB
    if password is not None:
        password = "Jet OLEDB:Database Password={password};".format(password=password)
    else:
        password = ""
    catalog.Create("Provider=Microsoft.ACE.OLEDB.12.0;Data source={path};Jet OLEDB:Engine Type=5;{password}".format(path=path, password=password))
    return "Provider=Microsoft.ACE.OLEDB.12.0;Data source={path};{password}".format(path=path, password=password)


'''
def remoteconfig(remote_path=True):
    global CONFIG, PAPERWORK, SQL, SENPAI, HISTORICY
    SENPAI = get_senpai()
    HISTORICY = get_historicy()
    kwargs = {"path": PATH}
    if remote_path is True:
        kwargs["remote_path"] = REMOTE_PATH
    CONFIG = Council(SENPAI.spreadsheet_open("SENPAI_Config"), name="Config", **kwargs)
    PAPERWORK = Council(SENPAI.spreadsheet_open("SENPAI_Paperwork"), name="Paperwork",
                        path=PATH, remote_path=REMOTE_PATH)
    SQL = Council(SENPAI.spreadsheet_open("SENPAI_Sql"), name="Sql", **kwargs)
'''