try:
    import senpai
    __package__ = "Senpai"
except ImportError:
    pass
    

import os
import sys
import asyncio
import base64
import comtypes, comtypes.client
import datetime
import difflib
import json
import re
import shelve
import string
import subprocess
import time
    
from .definitions import SENPAI, HISTORICY, CONFIG, LOCALPATH, SQL, PAPERWORK, REGEX, create_db
from decimal import Decimal
from events import *
from events import *
from threading import Thread, Lock, current_thread
from uAgentAPI.Wrapper import App, DefaultEventHandler
from Mordor import TheOneRing, Council, FellowshipOfTheRing, AlreadyServing, Lembas, LembasList, FlyYouFools
from multiprocessing import Pipe
from adohelper import Where, sql
from tempfile import TemporaryDirectory
from zashel.utils import copy, paste


# New Events
CloseServiceEvent = Event("close_service")
OpenServiceEvent = Event("open_service")
NewHistoryEvent = Event("new_history")
CommitmentsLoadedEvent = Event("commitments_loaded")
ParsedPaperEvent = Event("parsed_paper")
NewCustomerEvent = Event("new_customer")
GotUsersEvent = Event("got_users")
OpenCampaignEvent = Event("open_campaign")


#Exceptions
class uAgentError(Exception):
    """
    Base class to uAgent Errors
    """


class NotOpenAltitudeError(uAgentError):
    """
    Exception to raise in case altitude is not open
    """
    
    
def check_date(date):
    date = date.replace("-", "/")
    date = re.sub("/+", "/", date)
    items = date.split("/")
    now = datetime.datetime.now()
    if len(items) == 2:
        if now.month < 3 and int(items[1]) > 10:
            year = now.year - 1
        elif now.month > 10 and int(items[1]) < 3:
            year = now.year + 1
        else:
            year = now.year
        items.append(str(year))     
        if len(items[2]) == 2: items[2] = "20" + items[2]
    return datetime.datetime(int(items[2]), int(items[1]), int(items[0])).strftime("%d/%m/%Y")
    

PIPEIN, PIPEOUT = Pipe(False)
#uAgentHandler class
def uAgentHandler(pipe, pipe2=PIPEOUT):
    def uAgentHandler_wrapper(pipe):
        """
        Handler to events in pipe setted in uAgent
        :param pipe: pipe to receive data with
        :return: None
        """
        print(f"uAgentAPI PIPE Thread {current_thread()}*********************************")
        while True:
            try:
                event_name, data = pipe.recv()
            except (ConnectionRefusedError, ConnectionResetError):
                break
            else:
                print(f"En uAgent {event_name}")
                pipe2.send((event_name, data))
    return uAgentHandler_wrapper(pipe)


def FOOLShandler(): # Fly, you fools!
    print(current_thread())
    while True:
        try:
            event_name, data = PIPEIN.recv()
        except (ConnectionRefusedError, ConnectionResetError):
            print("Conexión rechazadaaaaa")
            break
        else:
            print(f"REGISTERING {event_name}")
            Gandalf.register_event("uAgent_"+event_name, raises=False)
            Gandalf.trigger_event("uAgent_"+event_name, *data)


#Persistent historic data
class Persistency(list): #DEPRECATED -> Not to save on Gdrive
    """
    Percistency class to save persistently data in HD and save a BU in GoogleDrive.
    """
    GOOGLE = HISTORICY
    def __init__(self, name, headers):
        """
        Initializes Persistency Object
        :param name: Name of the persistency object
        :param headers: Headers to use with each item.
        """
        list.__init__(self)
        self._name = name
        self._headers = headers
        self._spreadsheet = None
        self.remaining = int(CONFIG.SPREADSHEET_TOTAL_CELLS) # Amount of total cells in spreadsheets
        self.numeral = -1 # Numeral of spreadsheet, to look for it only once in a lifetime
        self.last_saved = -1 # Number indicating last saved item.
        #self._load_data()
        self.running = True
        #self.thread = Thread(target=self._listen)
        #self.thread.start()

    def __del__(self):
        """
        Closes object cleanly.
        :return: None.
        """
        self.running = False
        #self._save()

    def __getitem__(self, key):
        item = list.__getitem__(self, key)
        if isinstance(key, slice):
            return item
        else:
            return dict(zip(self.headers, item))

    @property
    def headers(self):
        """
        Returns the headers of each Percistency item.
        """
        return self._headers

    @property
    def name(self):
        """
        Returns name of the persistency object.
        """
        return self._name

    @property
    def path(self):
        """
        Returns path of shelve.
        """
        path = os.path.join(LOCALPATH, self.name)
        if not os.path.exists(LOCALPATH):
            os.makedirs(LOCALPATH, exist_ok=True)
        return path

    @property
    def spreadsheet(self):
        """
        Returns the spreadsheet dedicated to BU.
        """
        return self._get_spreadsheet()

    def _get_spreadsheet(self):
        """
        Returns current active and available spreadsheet.
        :return: zashel.gapi.Spreadsheets object
        """
        this_numeral = self.numeral
        remaining = self.remaining
        if self.numeral == 0:
            spreadsheets = self._get_spreadsheets()
            total = 1
            if len(spreadsheets) > 0:
                total = int(self.GOOGLE.spreadsheet_get_total_cells(name=spreadsheets[-1]))
            remaining = self.remaining - total
            this_numeral = len(spreadsheets) + 1
        name = self._get_spreadsheet_name(this_numeral)
        if remaining > 0 and self._spreadsheet is not None and self._spreadsheet.name != name:
            self._spreadsheet = self.GOOGLE.spreadsheet_open(name)
            self.remaining = remaining
            self.numeral = this_numeral
        elif remaining == 0 or self._spreadsheet is None:
            this_numeral += 1
            name = self._get_spreadsheet_name(this_numeral)
            try:
                self.GOOGLE.file_copy("Historic_Template", name)
            except FileNotFoundError:
                pass
            self.remaining = int(CONFIG.SPREADSHEET_TOTAL_CELLS)
            self.numeral = this_numeral
            time.sleep(1)
            while True:
                try:
                    time.sleep(1)
                    self._spreadsheet = self.GOOGLE.spreadsheet_open(name)
                except FileNotFoundError:
                    continue
                else:
                    break
        return self._spreadsheet

    def _get_spreadsheet_name(self, numeral):
        """
        Returns the name for a Persistency object and a given numeral.
        :param numeral: Numeral of file.
        :return: string containing the valid name of the spreadsheet.
        """
        numeral = "0" * (5 - len(str(numeral))) + str(numeral)
        return "_".join((os.environ["USERNAME"], self.name, numeral))

    def _get_spreadsheets(self):
        """
        Returns a list of spreadsheets of active user.
        :return: Ordered list of spreadsheets of active user.
        """
        spreadsheets = list(filter(lambda x: os.environ["USERNAME"]+"_"+self.name in x, self.GOOGLE.spreadsheets))
        spreadsheets.sort()
        spreadsheets.reverse()
        return spreadsheets

    def _listen(self):
        """
        Checks data and saves data to Drive when getting a specific amount of data.
        :return: None
        """
        while self.running is True:
            total = self.remaining > 50 and 50 or self.remaining
            if len(self) - self.last_saved >= total:
                self._save()
            time.sleep(1)
        print("Shutted down listen thread")

    def _load_data(self):
        """
        Loads Spreadsheet
        :return: None
        """
        spreadsheets = self._get_spreadsheets()
        final = list()
        for spreadsheet in spreadsheets[-2:]:
            final.extend([dict(zip(self.headers, eval(base64.b85decode(eval(item[0])).decode("utf-8"))))
                          for item in self.GOOGLE.spreadsheet_open(spreadsheet)["History"]
                          if item != ["Data"]])
        with shelve.open(self.path) as shelf:
            shelf["data"] = final[-1000:]
        self.clear()
        self.last_saved = len(final) - 1
        list.extend(self, final[-1000:])

    def _save(self):
        """
        Saves data to Spreadsheets.
        :return: None.
        """
        print("Saving historicy on GDrive")
        #last = self.last_saved >= 0 and self.last_saved or 0
        self.spreadsheet["History"].append_rows([[str(base64.b85encode(bytearray(json.dumps(item), "utf-8")))]
                                                                  for item in self[self.last_saved+1:]])
        self.last_save = len(self) - 1

    def append(self, data):
        """
        Overrides list append to save data in shelve.
        :param data: Data to save.
        :return: None.
        """
        if isinstance(data, (list, tuple)) and len(data) != len(self.headers):
            raise TypeError(f"Data list or tuple must have {len(self.headers)} items.")
        elif isinstance(data, dict) and not all([key in data for key in self.headers]):
            raise TypeError(f"Data dict must have {self.header} keys.")
        elif not isinstance(data, (list, tuple, dict)):
            raise TypeError("Data type must be dict, tuple or list")
        elif isinstance(data, dict) and all([key in data for key in self.headers]):
            data = [data[key] for key in self.headers]

        list.append(self, data)
        with shelve.open(self.path) as shelf:
            shelf["data"] = list(self)

    def extend(self, iterable):
        """
        Overrides list extend to save data in shelve.
        :param iterable: Data to save.
        :return: None.
        """
        for item in iterable:
            self.append(item)
        with shelve.open(self.path) as shelf:
            shelf["data"] = list(self)

    def save_and_exit(self):
        """
        Stops listen and saves all data
        :return: None
        """
        self.running = False
        self.thread.join()
        #self._save()


def parse_value(value): # Do not repeat yourself. Parse values to friendly sql string
    """
    Parses a value to insert it in an SQL.
    :param value: Value to parse
    :return: str
    """
    if isinstance(value, (datetime.datetime, datetime.timedelta)):
        return value.strftime("%d/%m/%Y %H:%M:%S")
    elif isinstance(value, float):
        return str(value).replace(".", ",")
    elif isinstance(value, Decimal):
        return value.to_eng_string().replace(".", ",")
    else:
        return str(value)


class Record(dict):
    def __init__(self, row, data, cursor, back_correlations, keys):
        #self.cursor = cursor
        self.back_correlations = back_correlations
        self.correlations = dict([(self.back_correlations[key], key) for key in self.back_correlations])
        self.keys = [key.lower() for key in keys]
        #self.row = row
        super().__init__(data)
        #for item in self.keys:
        #    self[item.lower()] = cursor.Fields.Item(self.keys.index(item)).Value
        self.update(data)

    def __getitem__(self, item):
        if isinstance(item, str):
            item = item.lower()
        if item in self:
            return dict.__getitem__(self, item)

    def __repr__(self):
        for key in self.keys:
            dict.__setitem__(self, key, self[key])
        return super().__repr__()

    def __str__(self):
        for key in self.keys:
            dict.__setitem__(self, key, self[key])
        return super().__str__()


class Row:
    totales = 0
    class Iter:
        def __init__(self, iterator):
            self.iterator = iterator
        def __next__(self):
            print("next")
            index = -1
            while True:
                index += 1
                try:
                    yield self.iterator[index]
                except KeyError:
                    print("STOP")
                    raise StopIteration()

    def __init__(self, conn, cursor, back_correlations):
        self.conn = conn
        self.cursor = cursor.Clone()
        self.back_correlations = back_correlations
        self.correlations = dict([(self.back_correlations[key], key) for key in self.back_correlations])
        self.fields = self.cursor.Fields
        self.keys = list()
        for index in range(self.fields.Count):
            key = self.fields.Item(index).Name
            if key in self.back_correlations:
                self.keys.append(self.back_correlations[key])
            else:
                self.keys.append(key)
        Row.totales += 1

    def __del__(self):
        self.cursor.Close()
        self.conn.Close()
        Row.totales -= 1

    def __getitem__(self, item):
        try:
            if isinstance(item, int):
                cursor = self.cursor
                if item >= 0:
                    cursor.MoveFirst()
                else:
                    item += 1
                    cursor.MoveLast()
                try:
                    cursor.Move(item)
                except comtypes.COMError:
                    raise KeyError()
                fields = cursor.Fields
                #for x in range(item):
                #    self.cursor.MoveNext()
                if cursor.EOF is False and cursor.BOF is False:
                    return Record(self,
                                  dict(zip(self.keys, [fields.Item(index).Value for index, key in enumerate(self.keys)])),
                                  cursor,
                                  self.back_correlations,
                                  self.keys)
        except comtypes.COMError:
            pass
        raise IndexError(f"{item} not found")

    def __len__(self):
        cursor = self.conn.execute(f"select count(*) as total from ({self.cursor.Source})")
        return int(cursor[1].Fields.Item(0).Value)

    def append(self, data):
        cursor = self.cursor.Clone()
        fields = cursor.Fields
        assert all([key in self.keys for key in data])
        keys = list(data.keys())
        keys.sort()
        cursor.AddNew([self.correlations[key] for key in keys], [data[key] for key in keys]) # It may not have to be ordered
        return Record(dict(zip(self.keys, [fields.Item(index).Value for index, key in enumerate(self.keys)])),
                      cursor,
                      self.back_correlations,
                      self.keys)


class Paperwork:
    CHECKED = False
    def __init__(self, name, correlations=None):
        """
        Initializes Paperwork
        :param name: name of the Paperwork
        :param definitions: dict with definition of the class.
            {"table": table,
             "insert_fields": fields separated by ", ",
             "update_fields": fields separated by ", ",
             "insert_user_field": field for user,
             "status": field for status,
             "insert_date", field for date od insertion,
             "update_user_field": field for user who updates data,
             "update_data": field for updated data date,
             "conn_type": type of connection,
             "conn": connection string if conn_type == connection_string }
        :param correlations: dict with correlation of variables.
            {"phone": "TLF",
             "invoice_date": "FECHA FACTURA",
             ... }
        """
        self._initialized = False
        self._name = name
        self._definitions = SQL[name]
        self._definitions["path"] = SQL["DB_NAMES"][self._definitions["data_source"]]
        if self._definitions["data_source"] in SQL["DB_PASSWORDS"]:
            self._definitions["password"] = f'Jet OLEDB:Database Password={SQL["DB_PASSWORDS"][self._definitions["data_source"]]};'
        else:
            self._definitions["password"] = ""
        self._definitions["conn"] = f"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={self._definitions['path']};" \
                                    f"{self._definitions['password']}"
        self._correlations = correlations
        if not Paperwork.CHECKED: # Debugged. 
            while True:
                try:
                    comtypes.CoInitialize()
                    try:
                        conn = comtypes.client.CreateObject("ADODB.Connection")
                    except OSError as e:
                        print(e)
                        raise
                    conn.Mode = 3  # Read/Write
                    cursor = comtypes.client.CreateObject("ADODB.RecordSet")
                    definitions = SQL["BUG_REPORTING_PAPER"]
                    definitions["path"] = SQL["DB_NAMES"][definitions["data_source"]]
                    if definitions["data_source"] in SQL["DB_PASSWORDS"]:
                        definitions["password"] = f'Jet OLEDB:Database Password={SQL["DB_PASSWORDS"][definitions["data_source"]]};'
                    else:
                        definitions["password"] = ""
                    definitions["conn"] = f"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={definitions['path']};" \
                                          f"{definitions['password']}"
                    conn.Open(definitions["conn"])
                except (comtypes.COMError): #THIS
                    if os.path.exists(SQL["DB_NAMES"]["Developing"]) is True:
                        print("install")
                        p = subprocess.Popen(["msiexec", "/i", os.path.join(os.path.split(sys.executable)[0], "AceRedist.msi"), 
                                              "/quiet", "/norestart", "/qn"])
                        p.wait()
                    else:
                        Paperwork.CHECKED = True # Raise Window
                else:
                    Paperwork.CHECKED = True
                    break
                finally:
                    try:
                        conn.close()
                    except (UnboundLocalError, comtypes.COMError):
                        pass
        self.initialize()

    def initialize(self):
        fields = ["table", "insert_fields", "update_fields", "insert_user_field", "status", "insert_date",
                  "update_user_field", "update_date", "conn"]
        if self._initialized is False:
            self._definitions = SQL[self._name]
            self._definitions["path"] = SQL["DB_NAMES"][self._definitions["data_source"]]
            if self._definitions["data_source"] in SQL["DB_PASSWORDS"]:
                self._definitions["password"] = f'Jet OLEDB:Database Password={SQL["DB_PASSWORDS"][self._definitions["data_source"]]};'
            else:
                self._definitions["password"] = ""
            self._definitions["conn"] = f"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={self._definitions['path']};" \
                                        f"{self._definitions['password']}"
            assert all([item in self._definitions for item in fields])
            self._table = self._definitions["table"]
            if isinstance(self._definitions["insert_fields"], (list, tuple, dict)):
                self._insert_fields = self._definitions["insert_fields"]
            elif isinstance(self._definitions["insert_fields"], str): # Legacy!
                self._insert_fields = [item.strip() for item in self._definitions["insert_fields"].split(",")]
            else:
                self._insert_fields = self._definitions["insert_fields"]
            if isinstance(self._definitions["update_fields"], (list, tuple, dict)):
                self._update_fields = self._definitions["update_fields"]
            elif isinstance(self._definitions["update_fields"], str): # Legacy!
                self._update_fields = [item.strip() for item in self._definitions["update_fields"].split(",")]
            else:
                self._update_fields = self._definitions["update_fields"]
            self._insert_user_field = self._definitions["insert_user_field"]
            self._status = self._definitions["status"]
            self._insert_date = self._definitions["insert_date"]
            self._update_user_field = self._definitions["update_user_field"]
            self._update_date = self._definitions["update_date"]
            self._conn = self._definitions["conn"]
            if self._correlations is not None and not "correlations" in self._definitions:
                pass
                # self._correlations = correlations
            elif "correlations" in self._definitions:
                if isinstance(self._definitions["correlations"], str): # Legacy!
                    self._correlations = eval(self._definitions["correlations"])
                else:
                    self._correlations = self._definitions["correlations"]
            else:
                self._correlations = dict(zip(self._insert_fields+self._update_fields,
                                              self._insert_fields+self._update_fields))
            self._correlations.update({"id": "Id",
                                       "insert_user": self._insert_user_field,
                                       "insert_date": self._insert_date,
                                       "status": self._status,
                                       "update_user": self._update_user_field,
                                       "update_date": self._update_date})
            self._correlations = dict([(key.lower(), self._correlations[key]) for key in self._correlations])
            self._back_correlations = dict([(self._correlations[key], key) for key in self._correlations])

    @property
    def conn(self):
        return self._conn

    @property
    def table(self):
        return self._table

    @property
    def update_user(self):
        return self._update_user_field

    def sqlexecute(self, template, *, values=None, fields=None, where=None, order=None, limit=0):
        """
        Executes sql with given data
        :param template: template to use between "SELECT", "INSERT", "UPDATE", "DELETE"
        :param values: list of values to set
        :param fields: list of fields to get/set
        :param where: list of tuples with (field, operator, value)
        :return: list of dicts if template == "SELECT", None by default
        """
        if self._initialized is False:
            self.initialize()
        if values is None:
            values = list()
        if fields is None:
            fields = "*"
        if where is None:
            where = list()
        old_values = values
        group = list()
        do_group = False
        values = list()
        for value in old_values:
            values.append(parse_value(value))
        del(old_values)
        for index, value in enumerate(values):
            if value is not None and value != "None" and value != "":
                values[index] = "'"+str(value)+"'"
            else:
                values[index] = "Null"
        #values = ["'"+value+"'" for value in values]
        if values is not None and fields is not None:
            fields_values = ", ".join([" = ".join(item) for item in zip(fields, values)])
        else:
            fields_values = ""
        values = ", ".join(values)
        for field in fields:
            if "(" in field:
                do_group = True
            else:
                group.append("["+field+"]")
        if fields != "*":
            fields = ", ".join(["(" not in field and "["+field+"]" or field for field in fields])
        table = "["+self._table+"]"
        if not isinstance(where, Where):
            old_where = where
            where = Where()
            for item in old_where:
                one = self._correlations[item[0]] # Key
                two = item[1] # Operator
                three = item[2] #This is the value
                where.And(one, two, three)
            del(old_where)
        else:
            def check_where(where):
                for index in range(len(where)): #Do not iterate and change it
                    if isinstance(where[index], Where):
                        where[index] = check_where(where[index])
                    else:
                        if isinstance(where[index], Where.Clause):
                            key = where[index][0].lower()
                            if key in self._correlations:
                                where[index][0] = Where.Clause.Operator(self._correlations[key])
                            elif key in [key.lower() for key in self._correlations.keys()]:
                                where[index][0] = Where.Clause.Operator(self._correlations[key.lower()])
                            elif key in self._back_correlations or key in [key.lower() for key in self._back_correlations.keys()]:
                                pass
                            else:
                                raise IndexError(f"{key} not in {table}")
                return where
            where = check_where(where)
        #where = " and ".join([" ".join(item) for item in where])
        where = where.sql
        sql_clause = eval("f\"" + SQL.__getattr__("GENERIC_"+template)+"\"")
        if order is not None:
            sql_clause += " order by "+", ".join(order)
        elif do_group is True and len(group) > 0:
            sql_clause += " group by "+", ".join(list(set(group)))
        if limit is not None and limit > 0 and sql_clause.startswith("select "):
            sql_clause = f"select top {limit} " + sql_clause[len("select "):]
        return self.sql_direct(sql_clause, template=template)

    def sql_direct(self, sql, *, template="SELECT"):
        if self._initialized is False:
            self.initialize()
        td = TemporaryDirectory()
        comtypes.CoInitialize()
        conn = comtypes.client.CreateObject("ADODB.Connection")
        conn.Mode = 3 # Read/Write
        cursor = comtypes.client.CreateObject("ADODB.RecordSet")
        print("A que se bloquea aquí")
        print(td.name)
        connstring = create_db(os.path.join(td.name, "temporary.mdb"))
        print(connstring)
        print(os.path.exists(os.path.join(td.name, "temporary.mdb")))
        conn.Open(connstring)
        print("Conectado")
        
        # Verificamos tipo de cursor que abrimos.
        # https://docs.microsoft.com/es-es/sql/ado/reference/ado-api/cursortypeenum
        # cursor = conn.execute(sql_clause)
        cursor.CursorType = 1 # Let's Try KeySet
        cursor.LockType = 2 # Pessimistic
        if self._definitions["data_source"] in SQL["DB_PASSWORDS"]:
            pwd = "PWD={}".format(SQL["DB_PASSWORDS"][self._definitions["data_source"]])
        else:
            pwd = ""
        if "insert into " not in sql:
            sql = sql.replace(" ["+self._table+"] ", f" [{self._table}] IN '' ';database={self._definitions['path']};{pwd}' ")
        else:
            sql = sql.replace(" ["+self._table+"] ", f" [;database={self._definitions['path']};{pwd}].[{self._table}] ")
        print(sql)
        cursor.Open(sql, conn)
        if template == "SELECT":
            return Row(conn, cursor, self._back_correlations)
        try:
            cursor.Close()
        except comtypes.COMError:
            pass
        conn.Close()
        td.cleanup()
        return None

    def get(self, filter=None, *, fields=None, order=["Id asc"], limit=0):
        """
        Gets data and return list of dictionary
        :param filter: list of tuples with (field, operator, value) or Where instance
        :return: list of dicts with data
        """
        if self._initialized is False:
            self.initialize()
        return self.sqlexecute("SELECT", where=filter, fields=fields, order=order, limit=limit)

    def get_insert_fields(self):
        """
        Gets list of insert_fields
        :return: list
        """
        if self._initialized is False: # initialize if is not (to solve bunny on paperwork)
            self.initialize()
        final = ["insert_user", "insert_date", "status"]
        for item in self._insert_fields:
            if item in self._back_correlations:
                final.append(self._back_correlations[item])
        return final

    def get_update_fields(self):
        """
        Gets list of update_fields
        :return: list
        """
        if self._initialized is False: # initialize if is not (to solve bunny on paperwork)
            self.initialize()
        final = ["status"]
        for item in self._update_fields:
            if item in self._back_correlations:
                final.append(self._back_correlations[item])
        return final

    def get_all_fields(self):
        insert_fields = self.get_insert_fields()
        del(insert_fields[insert_fields.index("status")])
        update_fields = self.get_update_fields()
        del(update_fields[update_fields.index("status")])
        if "update_comment" in update_fields:
            del(update_fields[update_fields.index("update_comment")])
        for field in update_fields:
            if field in insert_fields:
                del(insert_fields[insert_fields.index(field)])
        update_fields.append("update_comment")
        update_fields.append("status")
        return ["id"] + insert_fields + update_fields

    def post(self, data):
        """
        Inserts new data in table
        :param data: data in form of dictionary
        :return: None
        """
        if self._initialized is False:
            self.initialize()     
        print(list(self._correlations.keys()))
        assert all([key in self._correlations for key in data])
        mess = [(self._correlations[key], data[key]) for key in data]
        fields = list()
        values = list()
        for item in mess:
            fields.append(item[0])
            values.append(item[1])
        self.sqlexecute("INSERT", fields=fields, values=values)

    def put(self, filter, data):
        """
        Inserts given filter with given data
        :param filter: list of tuples with (field, operator, value) or Where instance
        :param data: data in form of dictionary
        :return: None
        """
        if self._initialized is False:
            self.initialize()
        assert all([key in self._correlations for key in data])
        mess = [(self._correlations[key], data[key]) for key in data]
        fields = list()
        values = list()
        for item in mess:
            fields.append(item[0])
            values.append(item[1])
        self.sqlexecute("UPDATE", fields=fields, values=values, where=filter)

    def delete(self, filter):
        """
        Deletes rows wich validates given filter
        :param filter: list of tuples with (field, operator, value) or Where instance
        :return: None
        """
        if self._initialized is False:
            self.initialize()
        return self.sqlexecute("DELETE", where=filter)


def get_connections():
    return Row.totales

    
Papers = {}
for item in SQL:
    if item.endswith("_PAPER"):
        Papers[item] = Paperwork(item)
DEBUG = Paperwork("BUG_REPORTING_PAPER")
COMMITMENTS_HELPER = Paperwork("COMMITMENTS_HELPER")


class CommitmentsHelper:
    def __init__(self, user, api):
        self.username = user
        self.helper = COMMITMENTS_HELPER
        self.api = api
        
    def complete(self, commitments, *, only=None):
        commitments = list(commitments)
        where1 = Where(("assigned", "=", self.username))
        where1.And("insert_date", ">=", datetime.datetime.now()-datetime.timedelta(days=10))
        if only is not None and isinstance(only, datetime.datetime):
            only = datetime.datetime(only.year, only.month, only.day)
            where11 = Where(("recall_date", ">=", only))
            where11.And("recall_date", "<", only + datetime.timedelta(days=1))
            where12 = Where(where11)
            where12.Or("recall_date", "is", "null")
            where1.And(where12)
        where2 = Where(("insert_user", "=", self.username))
        where2.And("insert_date", ">=", datetime.datetime.now()-datetime.timedelta(days=10))
        if only is not None and isinstance(only, datetime.datetime):
            where21 = Where(("recall_date", ">=", only))
            where21.And("recall_date", "<", only + datetime.timedelta(days=1))
            where22 = Where(where21)
            where22.Or("recall_date", "is", "null")
            where2.And(where22)
        where = Where(where1)
        where.Or(where2)
        assigned_data = self.helper.get(where)
        easycodes = [str(item["easycode"]) for item in assigned_data]
        if easycodes:
            easycodes="'"+"', '".join(easycodes)+"'"
            for campaign in self.api.open_campaigns:
                if "recobsorange" in campaign.lower():
                    commitments.extend(list(self.api.uAgent.execute(SQL.ORANGE_ASSIGNED_COMMITMENTS.format(easycodes=easycodes))))
                else:
                    commitments.extend(list(self.api.uAgent.execute(SQL.JAZZTEL_ASSIGNED_COMMITMENTS.format(easycodes=easycodes))))  

        help_commitments = dict()
        today = datetime.datetime.today()
        today = datetime.datetime(today.year, today.month, today.day)
        for item in commitments:
            if ("moment_agent" in item and int(item["code"]) in help_commitments and "moment_agent" in help_commitments[int(item["code"])] and
                    help_commitments[int(item["code"])]["moment_agent"] >= item["moment_agent"]):
                pass                
            else:
                help_commitments[int(item["code"])] = dict()
                for key in item:
                    help_commitments[int(item["code"])][key] = item[key] #Something is wrong with dict(item) and it may not be
        for item in assigned_data:
            if ("insert_date" in item and int(item["easycode"]) in help_commitments and 
                    "insert_date" in help_commitments[int(item["easycode"])] and 
                    help_commitments[int(item["easycode"])]["insert_date"] >= item["insert_date"]):
                pass
            elif int(item["easycode"]) in help_commitments:                    
                help_commitments[int(item["easycode"])].update(dict(item))
                help_commitments[int(item["easycode"])]["in_db"] = True
        for item in list(help_commitments.keys()):
            if (("recall_date" in help_commitments[item] and not help_commitments[item]["recall_date"]) or
                    "recall_date" not in help_commitments[item]):
                help_commitments[item]["recall_date"] = help_commitments[item]["moment_commitment"]
                if isinstance(help_commitments[item]["recall_date"], str):
                    help_commitments[item]["recall_date"] = datetime.datetime.strptime(help_commitments[item]["recall_date"], "%Y-%m-%d %H:%M:%S")
                if ("hour" in help_commitments[item] and help_commitments[item]["hour"] and 
                        "minute" in help_commitments[item] and help_commitments[item]["minute"]):
                    help_commitments[item]["recall_date"] += datetime.timedelta(hours=int(help_commitments[item]["hour"]),
                                                                                minutes=int(help_commitments[item]["minute"]))
                elif "hour" in help_commitments[item] and help_commitments[item]["hour"]:
                    help_commitments[item]["recall_date"] += datetime.timedelta(hours=int(help_commitments[item]["hour"]))
                if only is not None:
                    if help_commitments[item]["recall_date"] < only or help_commitments[item]["recall_date"] >= only+datetime.timedelta(days=1):
                        del(help_commitments[item])
                        continue
            if "in_db" not in help_commitments[item]:
                help_commitments[item]["in_db"] = False            
            if ("status" not in help_commitments[item] or (help_commitments[item]["recall_date"] < today and 
                    help_commitments[item]["status"] not in ("SEGUIMIENTO", "CUMPLIDO"))):
                if float(help_commitments[item]["amount_unpaid"].replace(",", ".")) == 0:
                    help_commitments[item]["status"] = "CUMPLIDO"
                elif help_commitments[item]["recall_date"].strftime("%d%m%Y") == datetime.datetime.today().strftime("%d%m%Y"):
                    help_commitments[item]["status"] = "PENDIENTE"
                elif help_commitments[item]["recall_date"] <= today:
                    help_commitments[item]["status"] = "INCUMPLE"
                else:
                    help_commitments[item]["status"] = "PENDIENTE"
            elif "status" not in help_commitments[item]:
                help_commitments[item]["status"] = "PENDIENTE"
            else:
                help_commitments[item]["status"] = help_commitments[item]["status"]
            #print(help_commitments[item])
        #copy(help_commitments)
        return sorted([help_commitments[key] for key in help_commitments], key=lambda x: x["recall_date"]) # This orders by recall_time


#The freaking API
class SenpaiAPI(FirstLightOnTheFifthDay):
    """
    API for senpai to implement in forward GUI
    """
    GOOGLE = SENPAI # Google api to spreadsheets and drive
    CONFIG = CONFIG # Configuration File
    #PAPERBACK = PAPERBACK # Paperback file

    def __init__(self, connected=True):
        """
        Initializes the API
        """
        #print("LOADING SenpaiAPI")
        #TheOneRing.__init__(self, "Senpai", manager_port=5506)
        FirstLightOnTheFifthDay.__init__(self)
        self.Gandalf_Status = True
        self._customers = {}
        if connected is True:
            self.uAgent = App()
            Thread(target=FOOLShandler, daemon=True).start()
            self.uAgent.set_handler(uAgentHandler)
        else:
            self.uAgent = None
        # Variables
        self._commitments = {} # Commitments list to show
        self._open_campaigns = [] # Campaigns open
        self._open_services = []
        self._phone_history = []
        self._customer_history = []
        self._custcode_history = []
        self.username = ""
        self.commitments_helper = None
        self.lock = Lock()
        self.altitude_lock = Lock()
        self.services = []
        self.re = {}
        self.prepare_regex()
        print(f"SenpaiAPI running in {current_thread()}")

    @property
    def commitments(self):
        """
        Returns self._commitments.
        """
        return self._commitments

    @property
    def customers(self):
        """
        Returns dictionary of customers by customer_id.
        """
        return self._customers

    @property
    def customer_history(self):
        """
        Resturns Persistency instance of customer history.
        """
        return self._customer_history

    @property
    def custcode_history(self):
        """
        Resturns Persistency instance of customercode history.
        """
        return self._custcode_history
        
    @property
    def open_campaigns(self):
        """
        Returns list of open campaigns in uAgent.
        """
        return self._open_campaigns
        
    @property
    def open_services(self):
        """
        Returns list of open services in uAgent.
        """
        return self._open_services

    @property
    def phone_history(self):
        """
        Returns Persistency instance of phone history.
        """
        return self._phone_history

    def exit(self):
        """
        It necesarily might exit cleanly.
        :return: None.
        """
        if self.uAgent is not None:
            try:
                self.uAgent.shutdown()
            except (comtypes.COMError, ConnectionRefusedError):
                pass
                #self._phone_history.save_and_exit()
                #self._customer_history.save_and_exit()

    def get_active_session_data(self):
        #TODO
        pass

    def call_traffic(self,traffic_ext):
        print(f"TRAFFIC:{traffic_ext}")
        if self.uAgent is not None:
            try:
                self.uAgent.extend(traffic_ext)
            #except comtypes.COMError:
            except:
                print("direct call")
                self.uAgent.call_direct(traffic_ext)

    def get_all_commitments(self, service):
        """
        Returns all active and pending commitments.
        :param service: Commitments service.
        :return: List of active and pending commitments.
        """
        if self.uAgent is not None:
            sql = SQL[f"{service}_ALL_COMMITMENTS"]
            return self.uAgent.execute(sql)

    def get_all_my_commitments(self):
        """
        Gets all commitments for today in every open campaign and returns them in a list.
        :return: list with every commitment.
        """
        print("Getting all my commitments")
        if self.uAgent is not None:
            self._commitments = dict()
            for service in self.open_services:
                self._commitments[service] = self.commitments_helper.complete(self.get_all_user_commitments(service))
            return self.commitments

    def get_all_my_commitments_for_today(self):
        """
        Gets all commitments for today in every open campaign and returns them in a list.
        :return: list with every commitment.
        """
        print("Getting all my commitments")
        self._commitments = dict()
        if self.uAgent is not None:
            for service in self.open_services:
                now = datetime.datetime.now()
                now = datetime.datetime(now.year, now.month, now.day)
                self._commitments[service] = self.commitments_helper.complete(self.get_my_commitments_for_today(service), only=now)
            return self.commitments

    def get_session_data(self, session_id):
        #TODO
        pass

    def get_config(self):
        """
        Getter to CONFIG to use with managers
        :return: API.CONFIG
        """
        return self.CONFIG

    def get_google(self):
        """
        Getter to GOOGLE to use with managers
        :return: API.GOOGLE
        """
        return self.GOOGLE

    def get_my_commitments(self, service):
        """
        Returns all active and pending commitments of active uAgent user.
        :param service: Commitments service.
        :return: List of active and pending commitments of active uAgent user.
        """
        print("Get_MY_Commitments, ", service)
        if self.uAgent is not None:
            sql = SQL[f"{service}_MY_COMMITMENTS"]
            data = self.uAgent.execute(sql, [self.uAgent.is_logged]*(len(sql.split("?"))-1))
            print(f"Retreive {len(data)} commitments")
            return data
            return list()

    def get_all_user_commitments(self, service):
        """
        Returns all active and pending commitments of active uAgent user.
        :param campaign: Commitments campaign.
        :return: List of active and pending commitments of active uAgent user.
        """
        print("Get_All_MY_Commitments, ", service)
        if self.uAgent is not None:
            sql = SQL[f"{service}_MY_COMMITMENTS"]
            data = self.uAgent.execute(sql, [self.uAgent.is_logged]*(len(sql.split("?"))-1))
            #data = self.uAgent.execute(sql, ["ops29dg25"]*(len(sql.split("?"))-1)) #debunny commitments
            print(f"Retreive {len(data)} commitments")
            return data

    def get_my_commitments_for_today(self, service):
        """
        Returns all active and pending commitments of active uAgent user for today.
        :param campaign: Commitments campaign.
        :return: List of active and pending commitments of active uAgent user for today.
        """
        print("Get_MY_Commitments_For_Today, ", service)
        if self.uAgent is not None:
            sql = SQL[f"{service}_MY_COMMITMENTS_FOR_TODAY"]
            data = self.uAgent.execute(sql, [self.uAgent.is_logged]*(len(sql.split("?"))-1))
            #data = self.uAgent.execute(sql, ["ops29dg25"]*(len(sql.split("?"))-1))
            print(f"Retreive {len(data)} commitments")
            return data

    def get_my_expired_commitments(self, service):
        """
        Returns every expired commitment for active user.
        :param campaign: Commitments campaign.
        :return: List of expired commitments.
        """
        if self.uAgent is not None:
            sql = SQL[f"{service}_MY_EXPIRED_COMMITMENTS"]
            data = self.uAgent.execute(sql, [self.uAgent.is_logged]*(len(sql.split("?"))-1))
            return data
            print(f"Retreive {len(data)} commitments")

    def get_my_fulfilled_commitments(self, service): # Me suena mal "fulfilled commitments"
        """
        Returns every expired commitment for active user.
        :param campaign: Commitments campaign.
        :return: List of expired commitments.
        """
        if self.uAgent is not None:
            sql = SQL[f"{service}_MY_FULFILLED_COMMITMENTS"]
            data = self.uAgent.execute(sql, [self.uAgent.is_logged]*(len(sql.split("?"))-1))
            return data
            print(f"Retreive {len(data)} commitments")

    def get_my_special_actions(self): #TODO
        """
        Returns every Special Action and what it's done with it
        """
        if any("RecobsOrange" in campaign for campaign in self.open_campaigns):
            today = datetime.datetime.today()
            today = datetime.datetime(today.year, today.month, today.day)
            where = [["insert_user", "=", self.username], ["insert_date", ">=", today-datetime.timedelta(days=30)]]
            data = Papers["BO_ORANGE_INFOSMS_PAPER"].get(where)
            return data
        else:
            pass

    def get_my_special_actions_performance(self): #TODO
        """
        Returns every Special Action and what it's done with it
        """
        if any("RecobsOrange" in campaign for campaign in self.open_campaigns):
            today = datetime.datetime.today()
            this_month = today.month
            this_year = today.year
            last_month = this_month > 0 and this_month - 1 or 12
            last_year = last_month == 12 and this_year - 1 or this_year
            next_month = this_month < 12 and this_month + 1 or 1
            next_year = next_month == 1 and this_year + 1 or this_year

            initial = datetime.datetime(last_year, last_month, 1).strftime("%Y-%m-%d")
            middle = datetime.datetime(this_year, this_month, 1).strftime("%Y-%m-%d")
            end = datetime.datetime(next_year, next_month, 1).strftime("%Y-%m-%d")

            sql = f"select SEGMENTO as segmento, sum(IMPORTE_PAGADO) as amount from [{SQL['BO_ORANGE_INFOSMS_PAPER']['table']}] where " \
                  "FECHA_DE_PAGO >= #{}# and FECHA_DE_PAGO < #{}# and TSFCP = '{}' and COMPUTA = 'OK' group by SEGMENTO"
            
            data = {"last": Papers["BO_ORANGE_INFOSMS_PAPER"].sql_direct(sql.format(initial, middle, self.username)),
                    "current": Papers["BO_ORANGE_INFOSMS_PAPER"].sql_direct(sql.format(middle, end, self.username))}

            return data
        else:
            pass            

    def get_paperback(self):
        """
        Getter to PAPERBACK to use with managers
        :return: API.PAPERBACK
        """
        return self.PAPERBACK

    def get_uAgent(self):
        """
        Getter to uAgent to use with managers
        :return: self.uAgent
        """
        return self.uAgent

    def get_users(self, *, fullname=None, usr_name=None, login=None, only_loggedin=False):
        if self.uAgent is not None:
            where = Where()
            if fullname is not None:
                [where.And("fullname", "like", item.strip(" ")) for item in fullname.split(" ")]
            if usr_name is not None:
                where.And("usr_name", "=", usr_name)
            if login is not None:
                where.And("position_id", "=", login)
            if only_loggedin is True:
                where.And("logged_as_agent", "=", 1)
            data = self.uAgent.execute(SQL.SEARCH_FOR_USER.format(where = "where "+sql(where)))
            print(f"Encontrados {len(data)} usuarios con esos datos")
            GotUsersEvent(data)

    def historicy_session(self, session_id, session_data):
        #TODO
        pass

    def initialize(self):
        """
        Initializes SenpaiAPI.
        :return: None
        """
        pass

    def login(self, username, password=None, extension=None, *, site="Madrid", instance="sesmapaltas01:1500"): # TODO: Make it configurable
        """
        Searches for an open instance of altitude and attaches it.
        #TODO: in case of shitty altitude shutdown, reinitilize the uAgent app
        :param username: username to altitude
        :param password: password to altitude
        :return: None
        """
        self.username = username
        print(username)
        if self.uAgent is not None:
            self.uAgent.open_uAgent_and_login(username=username, password=password, instance=instance, extension=extension,
                                              site=site)
            self.commitments_helper = CommitmentsHelper(self.username, self)
        else:
            self.commitments_helper = None
        NewCustomerEvent({"insert_user": username}) # Chapuza
        
    def make_re_papers(self):
        """
        Prepares the regular expressions for papers
        """
        self.re["papers"] = {}
        self.paper_keywords = {}
        for service in self.services:
            for paper in PAPERWORK[service]:
                if paper in SQL:
                    if "keywords" in SQL[paper]:
                        for string in SQL[paper]["keywords"]:
                            if string not in self.re["papers"]:
                                self.re["papers"][string] = re.compile(r"\W("+"\W*".join([item[0]+"\w+" for item in string.split(" ")])+")", re.IGNORECASE)
                            if string not in self.paper_keywords:
                                self.paper_keywords[string] = []
                            self.paper_keywords[string].append((service, paper))
        #print(self.re["papers"])

    def is_logged(self):
        '''
        Return the name of the Altitude logged agent
        :return: Login
        '''
        if self.uAgent is not None:
            return self.uAgent.is_logged
        else:
            return None

    def prepare_opening(self):
        if self.uAgent is not None:
            self.uAgent.prepare_opening()

    def save_paperback(self, paperback, data): # Deprecated
        """
        Saves data in paperback.
        :param paperback: Type of data to be saved.
        :param data: Data to save.
        :return: None.
        """
        keys = PAPERBACK[paperback]
        final = dict()
        for key in keys:
            if key in data:
                to_save = data[key]
            else:
                to_save = ""
            final[key] = to_save
            final["user"] = os.environ["USERNAME"]
            final["date"] = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        files = list(filter(lambda x: paperback in x, self.GOOGLE.files))
        files.sort()
        files.reverse()
        new = None
        if len(files) > 1:
            while True:
                try:
                    spreadsheet = self.GOOGLE.spreadsheet_open(files[1]) # Files[0] May be a template
                except FileNotFoundError:
                    time.sleep(1)
                    continue
                else:
                    break
        else:
            new = 0
        if new is None and spreadsheet.get_total_cells() > CONFIG.SPREADSHEET_TOTAL_CELLS:
            new = int(files[1].split("_")[-1]) + 1
        if new is not None:
            name = "_".join((paperback, "0"*(5-len(str(new)))+str(new)))
            self.GOOGLE.file_copy(files[0], name)
            time.sleep(1.5)
        while True:
            try:
                spreadsheet = self.GOOGLE.spreadsheet_open(name)  # Files[0] May be a template
            except FileNotFoundError:
                time.sleep(1)
                continue
            else:
                break
        spreadsheet.append_row([str(base64.b85encode(bytearray(json.dumps(final), "cp1252"))), "PENDIENTE"])

    def update_my_commitments_for_today(self):
        """
        Updates the commitments in self.commitments.
        :return: None.
        """
        self._commitments = self.get_all_my_commitments_for_today()
        if len(self._commitments) == 0:
            print("ni uno")
            self._commitments = dict()
        CommitmentsLoadedEvent(json.dumps(self.commitments, default=str))

    def update_all_my_commitments(self):
        """
        Updates the commitments in self.commitments.
        :return: None.
        """
        self._commitments = self.get_all_my_commitments()
        if len(self._commitments) == 0:
            print("ni uno")
            self._commitments = dict()
        CommitmentsLoadedEvent(json.dumps(self.commitments, default=str))

    def search_by_cod_bscs(self, external_id):
        """
        Search phones and customer_id of customer by the cod_bscs
        :param external_id: cod_bscs for orange or idCliente for Jazztel
        :return: dict (uAgentAPI.Row) with result of SQL query
        """
        if self.uAgent is not None:
            data = self.uAgent.execute(SQL.ORANGE_SEARCH_BY_COD_BSCS,[external_id])
            return data

    def search_by_dni(self, customer_id):
        """
        Search phones and customer_id of customer by the cod_bscs
        :param customer_id: customer_id for Jazztel
        :return: dict (uAgentAPI.Row) with result of SQL query
        """
        if self.uAgent is not None:
            data = self.uAgent.execute(SQL.JAZZTEL_SEARCH_BY_DNI,[customer_id])
            return data
            
    def prepare_regex(self):
        self.regex = {}
        for regex in REGEX:
            self.regex[regex] = re.compile(REGEX[regex]['value'], REGEX[regex]["ignorecase"]=="on" and re.IGNORECASE) #added ['value'] -> and ignorecase

    # Events
    def _event_clipboard_data(self, data):
        """
        Gets data from clipboard event.
        :param data: Data saved in clipboard.
        :return: None.
        """
        # Making order in this shit
        original = data
        data = " "+data.replace("*", " ").replace("\r", "").replace("\n", " ")+" " #Quitamos los asteriscos
        final = {}
        for regex in self.regex:
            print(f"{regex}, {self.regex[regex]}")
            final[regex] = self.regex[regex].findall(data)
            print(f"{final[regex]}")
        final["all"] = original
        final["keyword"] = {} #change to dict, cause the list does not work
        if "papers" in self.re:
            for paper in self.re["papers"]:
                item = self.re["papers"][paper].findall(' ' + data)
                def normalize(d):
                    '''
                    Normalizes data in this place
                    :param d: list with data
                    :return: list
                    '''
                    final = []
                    for item in d:
                        if not isinstance(item, (tuple, list)):
                            final.append(item.lower())
                        else:
                            final.extend(normalize(item))
                    return final
                item = normalize(item)
                #print(f"{paper}: {item}")
                d = difflib.get_close_matches(paper.lower(), item, cutoff=0.85)
                print(f"Difflib: {d}")
                if len(d) > 0:
                    final["keyword"][paper] = d
        ParsedPaperEvent(final) # It may be all in this way...
        
    def _event_close_service(self, service):
        """
        Event for a closing service
        """
        if service in self.services:
            del(self.services[self.services.index(service)])
            self.make_re_papers()
        
    def _event_open_service(self, service, campaign):
        """
        Event for new service open.
        :param service: service open.
        :return: None.
        """
        if service not in self.services:
            self.services.append(service)
            self.make_re_papers()
            
    def _event_config_file_changed(self, name):
        print(f"Changed {name}")
        if name.lower() == "sql":
            self.make_re_papers()
        elif name.lower() == "regular expressions":
            self.prepare_regex()

    def _event_commitments_loaded(self, commitments):
        print("Commitments gotten")

    def _event_uAgent_SessionContactLoadedEvent(self, sessionId, data):
        """
        Event for uAgent_SessionContactLoadedEvent.
        """
        print(f"SessionContactLoadedEvent {sessionId} : {data}")

    def _event_uAgent_SessionPhoneEvent(self, sessionId, data):
        """
        Event for uAgent_SessionPhoneEvent.
        :param sessionId: SessionId of phone event.
        :param data: Data relative to phone event.
        :return: None.
        """
        print(f"SessionPhoneEvent {sessionId}: {data}")
        with self.lock:
            self.inbound_phone = data['SessionPhoneEvent']['DestinationNumber']
        print(f"Inbound Phone: {self.inbound_phone}")
        # State 1: Llamada atendida
        # State 5:

    def _event_uAgent_CampaignFeatureEvent(self, campaign, state):
        """
        Event for uAgent_CampaignFeatureEvent.
        :param campaign: Campaign affected.
        :param state: New state. Open, Close, SignOn, SignOff, Ready, NotReady and Unknown.
        :return: None.
        """
        print(campaign, state)
        services = CONFIG.SERVICES
        if state == "Open":
            with self.lock:
                self.open_campaigns.append(campaign)
                OpenCampaignEvent(campaign)
            for service in services:
                print(service)
                print(services[service])
                if any([s==campaign for s in services[service]]) and not service in self.open_services:
                    self.open_services.append(service)
                    OpenServiceEvent(service, campaign)
                else:
                    pass
        elif state == "Close":
            with self.lock:
                del(self._open_campaigns[self.open_campaigns.index(campaign)])
            for service in services:
                if (any([s==campaign for s in services[service]]) and 
                        not any([item in services[service] for item in self.open_campaigns if item != campaign])):
                    del(self.open_services[self.open_services.index(service)])
                    CloseServiceEvent(service)

